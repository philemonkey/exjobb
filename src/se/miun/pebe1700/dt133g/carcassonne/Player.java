package se.miun.pebe1700.dt133g.carcassonne;

import java.util.Stack;

import se.miun.pebe1700.dt133g.carcassonne.enums.Feature;
import se.miun.pebe1700.dt133g.carcassonne.enums.Meeple;
import se.miun.pebe1700.dt133g.utils.StackPushPullException;

public class Player {

	private Meeple colour;
	private int score;
	private Stack<Meeple> meeples;
	public final int MEEPLE_MAX = 7;
	private int meepleBroughtBack;
	private int highestScore;
	
	public Player(Meeple colour) {
		this.meepleBroughtBack = 0;
		this.highestScore = 0;
		this.colour = colour;
		this.score = 0;
		meeples = new Stack<>();
		for(int i = 0 ; i < MEEPLE_MAX ; i++) {
			this.meeples.push(this.colour);
		}
	}
	
	public Player(Player player) {
		this.meepleBroughtBack = player.getMeepleBroughtBack();
		this.highestScore = player.getHighestScore();
		this.colour = player.getColour();
		this.score = player.getScore();
		meeples = new Stack<>();
		for(int i = 0 ; i < player.getRemainingMeeples() ; i++) {
			this.meeples.push(this.colour);
		}
	}
	
	public void pushMeeple() throws StackPushPullException {
		if(this.meeples.size() == MEEPLE_MAX) {
			throw new StackPushPullException("can't push to beyound max size: " + MEEPLE_MAX);
		}
		this.meeples.push(this.colour);
		meepleBroughtBack++;
	}
	
	public Meeple popMeeple() throws StackPushPullException {
		if(meeples.empty()) {
			throw new StackPushPullException("can't pop empty stack");
		}
		return this.meeples.pop();
	}
	
	public int getMeepleBroughtBack() {
		return this.meepleBroughtBack;
	}
	
	public void setHighestScore(int score) {
		highestScore = score > highestScore ? score : highestScore;
	}
	
	public int getHighestScore() {
		return this.highestScore;
	}
	
	public int getRemainingMeeples() {
		return this.meeples.size();
	}
	
	public void addScore(int n) {
		this.score += n;
	}
	
	public int getScore() {
		return this.score;
	}
	
	public Meeple getColour() {
		return this.colour;
	}
	
	@Override
    public boolean equals(Object o) {  
        if (o == this) { 
            return true; 
        } 
        if (!(o instanceof Player)) { 
            return false; 
        } 
        Player p = (Player) o; 
        return this.colour == p.getColour();
    } 
	
	
}
