package se.miun.pebe1700.dt133g.mcts.ph;

public class ScoreVisits {

	private double score;
	private int visits;
	public ScoreVisits() {
		score = visits = 0;
	}
	
	public void incrementScore(double n) {
		score += n;
	}
	
	public void incrementVisits(int n) {
		visits += n;
	}
	
	public double getScore() {
		return this.score;
	}
	
	public int getVisits() {
		return this.visits;
	}
	

}
