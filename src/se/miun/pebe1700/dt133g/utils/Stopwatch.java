package se.miun.pebe1700.dt133g.utils;

import java.util.concurrent.TimeUnit;

public class Stopwatch {

	private long start;
	private long stop;
	private long total;
	public Stopwatch() {
		total = 0;
	}
	
	public void start() {
		start = System.nanoTime();
	}
	
	public void stop() {
		stop = System.nanoTime();
	}
	
	public void halt() {
		total += System.nanoTime()-start;
	}

	public double getTotal(TimeUnit T) {
		double time = total;
		switch(T) {
		case NANOSECONDS : break;
		case MICROSECONDS : time /=1000; break;
		case MILLISECONDS : time /=1000000; break;
		case SECONDS : time /=1000000000; break;
		default : time = -1;
		}
		return time;
	}
	
	
	public double getTime(TimeUnit T) {
		double time = stop-start;
		switch(T) {
		case NANOSECONDS : break;
		case MICROSECONDS : time /=1000; break;
		case MILLISECONDS : time /=1000000; break;
		case SECONDS : time /=1000000000; break;
		default : time = -1;
		}
		return time;
	}
	
	public void clear() {
		start = stop = total = 0;
	}
}
