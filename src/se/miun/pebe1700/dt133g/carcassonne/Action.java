package se.miun.pebe1700.dt133g.carcassonne;

import se.miun.pebe1700.dt133g.carcassonne.TerrainTile;
import se.miun.pebe1700.dt133g.carcassonne.enums.Direction;

public class Action {
	
	
	private TerrainTile tile;
	private Direction meepleDirection;
	
	public Action(TerrainTile tile, Direction meepleDirection) {
		this.tile = tile;
		this.meepleDirection = meepleDirection;
	}
	
	public TerrainTile getTile() {
		return this.tile;
	}
	
	public Direction getMeepleDirection() {
		return this.meepleDirection;
	}
	
	public String toString() {
		return this.tile.getPosition().toString() + " meeple: " + this.meepleDirection; 
	}

}
