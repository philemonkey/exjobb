package se.miun.pebe1700.dt133g.carcassonne.gui;

import se.miun.pebe1700.dt133g.mcts.RewardPolicy;

public class AgentSetup {

	public final int ITERATIONS;
	public final int TIME_LIMIT;
	public final int DETERMINIZATIONS;
	public final int THREADS;
	public final boolean RANDOM_DECK;
	public final String AGENT_TYPE;
	public final RewardPolicy REWARD_POLICY;
	
	
	public AgentSetup(int iter, int time, int deter, int threads, boolean random, String agent, RewardPolicy rewardPolicy) {
		ITERATIONS = iter;
		TIME_LIMIT = time;
		DETERMINIZATIONS = deter;
		THREADS = threads;
		RANDOM_DECK = random;
		AGENT_TYPE = agent;
		REWARD_POLICY = rewardPolicy;
	}
}
