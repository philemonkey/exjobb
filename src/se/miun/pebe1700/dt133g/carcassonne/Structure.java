package se.miun.pebe1700.dt133g.carcassonne;

import java.util.ArrayList;
import java.util.List;

import se.miun.pebe1700.dt133g.carcassonne.enums.Feature;
import se.miun.pebe1700.dt133g.carcassonne.enums.Meeple;

public class Structure {
    private List<Meeple> meeples;
    private List<PartialStructure> partialStructures;
    private int structureSize;
    private Feature feature;
    private boolean structureDone;

    public Structure(Feature feature) {
    	structureDone = true;
    	this.feature = feature;
        structureSize = 0;
        partialStructures = new ArrayList<>();
        meeples = new ArrayList<>();
    }
    
    public void addToSize(int n) {
    	structureSize += n;
    }
    
    // This shouldn't be accessible to Client.
    public void setStructureDone(boolean done) {
    	this.structureDone = done;
    }
    
    public boolean getStructureDone() {
    	return this.structureDone;
    }
    
    public void addMeeple(Meeple m) {
    	meeples.add(m);
    }
    
    public Feature getFeature() {
    	return this.feature;
    }
    
    public int getSize() {
    	return structureSize;
    }
    
    public List<Meeple> getMeeples() {
    	return meeples;
    }
    
    public List<Meeple> clearMeeples() {
    	for(PartialStructure ps: partialStructures) {
    		ps.removeMeeple();
    	}
    	
    	try {
    		List<Meeple> deepCopy = new ArrayList<>();
    		deepCopy.addAll(meeples);
    		return deepCopy;
        } finally {
        	meeples.clear();
        }
    }
    
    public void addPartialStructure(PartialStructure ps) {
    	partialStructures.add(ps);
    }
    
};
