package se.miun.pebe1700.dt133g.carcassonne;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import se.miun.pebe1700.dt133g.carcassonne.enums.Direction;
import se.miun.pebe1700.dt133g.carcassonne.enums.Feature;
import se.miun.pebe1700.dt133g.carcassonne.enums.Meeple;
import se.miun.pebe1700.dt133g.utils.Point;

public class TerrainTile extends Tile {

	private CircularList<Feature> baseFeatures;
	private Feature centerFeature;
	private Map<Direction, Feature> directionFeature;
	// private Map<Direction, Feature> cornerDirectionFeature;
	private List<PartialStructure> structures;
	private Meeple meepleOnTile;
	private Direction meepleDirection;

	public TerrainTile(CircularList<Feature> baseFeatures, Feature centerFeature, List<PartialStructure> structures) {
		this.baseFeatures = baseFeatures;
		this.centerFeature = centerFeature;
		this.structures = structures;
		init();
	}

	public TerrainTile(TerrainTile tile) {

		this.baseFeatures = new CircularList<>(4);
		for (Feature feat : tile.getBaseFeatures()) {
			this.baseFeatures.add(feat);
		}
		this.centerFeature = tile.centerFeature;
		this.structures = tile.getPartialStructuresDeepCopy();

		if (tile.getPosition() != null) {
			this.position = new Point(tile.getPosition());
		}
		this.meepleOnTile = tile.getMeepleOnTile();

		directionFeature = new HashMap<>();
		for (int i = 0; i < 4; i++) {
			directionFeature.put(Utils.BASE_DIRECTIONS[i], tile.getFeatureFromDirection(Utils.BASE_DIRECTIONS[i]));
		}

		for (int i = 0; i < 4; i++) {
			neighbours.put(Utils.BASE_DIRECTIONS[i], tile.getNeighbourInDirecton(Utils.BASE_DIRECTIONS[i]));
		}

	}

	private void init() {
		directionFeature = new HashMap<>();
		for (int i = 0; i < 4; i++) {
			directionFeature.put(Utils.BASE_DIRECTIONS[i], baseFeatures.at(i));
		}

		for (int i = 0; i < 4; i++) {
			neighbours.put(Utils.BASE_DIRECTIONS[i], new EmptyTile(true));
		}

	}

	public CircularList<Feature> getBaseFeatures() {
		return baseFeatures;
	}

	public Feature getCenterFeature() {
		return centerFeature;
	}

	public void rotateTile(int k) {
		baseFeatures.leftShift(k);

		directionFeature = new HashMap<>();
		for (int i = 0; i < 4; i++) {
			directionFeature.put(Utils.BASE_DIRECTIONS[i], baseFeatures.at(i));
		}

		for (PartialStructure part : structures) {
			part.leftShiftDirections(k);
		}

	}

	public String toString() {
		String s = "";

//		for (Map.Entry<Direction, List<Feature>> entry : directionFeature
//				.entrySet()) {
//			s += "[" + entry.getKey() + ":" + entry.getValue() + "]";
//		}
		return s;
	}

	public Feature getFeatureFromDirection(Direction dir) {
		if (dir == Direction.CENTER) {
			return centerFeature;
		} else
			return directionFeature.get(dir);
	}

	public List<PartialStructure> getPartialStructures() {

		return Collections.unmodifiableList(structures);
	}

	public List<PartialStructure> getPartialStructuresDeepCopy() {

		List<PartialStructure> deepCopy = new ArrayList<>();
		for (PartialStructure s : structures) {
			PartialStructure struct = new PartialStructure();
			for (Direction dir : s.getDirections()) {
				struct.addDirection(dir);
			}
			struct.addFeature(s.getFeature());
			struct.setMeeple(s.getMeeple(), s.getMeepleDirection());
			deepCopy.add(struct);
		}
		return deepCopy;
		// return Collections.unmodifiableList(structures);
	}

	public boolean placeMeeple(Meeple color, Direction dir) {
		for (PartialStructure part : structures) {

			if (part.getMeeple() == null) {
				for (Direction d : part.getDirections()) {
					if (d == dir) {
						part.setMeeple(color, dir);
						this.meepleOnTile = color;
						this.meepleDirection = dir;
						return true;
					}
				}
			}

		}
		return false;
	}

	public Meeple getMeepleOnTile() {
		return meepleOnTile;
	}

	public Direction getMeepleDirection() {
		return meepleDirection;
	}

	public void removeMeeple() {
		meepleOnTile = null;
		meepleDirection = null;
		for (PartialStructure ps : this.getPartialStructures()) {
			ps.removeMeeple();
		}
	}

	public boolean sameTile(TerrainTile tile) {
		for (int i = 0; i < 4; i++) {
			if (tile.getFeatureFromDirection(Utils.BASE_DIRECTIONS[i]) != this
					.getFeatureFromDirection(Utils.BASE_DIRECTIONS[i])) {
				return false;
			}
		}
		if (this.centerFeature != tile.getCenterFeature())
			return false;
		if ((this.getMeepleDirection() == null) != (tile.getMeepleDirection() == null)) {
			return false;
		}
		if (this.getMeepleDirection() != null) {
			if (this.getMeepleOnTile() != tile.getMeepleOnTile()) {
				return false;
			}
			if (this.getMeepleDirection() != tile.getMeepleDirection()) {
				return false;
			}

		}
		if (!this.position.equals(tile.getPosition())) {
			return false;
		}
		return true;
	}

	public boolean sameTileUnregardPosition(TerrainTile tile) {
		for (int i = 0; i < 4; i++) {
			if (tile.getFeatureFromDirection(Utils.BASE_DIRECTIONS[i]) != this
					.getFeatureFromDirection(Utils.BASE_DIRECTIONS[i])) {
				return false;
			}
		}
		if (this.centerFeature != tile.getCenterFeature())
			return false;
		if ((this.getMeepleDirection() == null) != (tile.getMeepleDirection() == null)) {
			return false;
		}
		if (this.getMeepleDirection() != null) {
			if (this.getMeepleOnTile() != tile.getMeepleOnTile()) {
				return false;
			}
			if (this.getMeepleDirection() != tile.getMeepleDirection()) {
				return false;
			}

		}
		return true;
	}

	public boolean sameTileUnregardPositionAndRotation(TerrainTile tile) {

		TerrainTile tileCopy = new TerrainTile(tile);
		if (this.centerFeature != tileCopy.getCenterFeature())
			return false;

		if ((this.meepleDirection == null) != (tile.getMeepleDirection() == null)) {
			return false;
		}

		int rotations = 0;

		boolean match = false;
		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 4; j++) {
				if (tileCopy.getFeatureFromDirection(Utils.BASE_DIRECTIONS[i]) != this
						.getFeatureFromDirection(Utils.BASE_DIRECTIONS[i])) {
					break;
				}
				if (j == 3) {
					match = true;
				}
			}
			if (!match) {
				tileCopy.rotateTile(1);
				rotations++;
			} else {
				break;
			}
		}
		if (match) {
			if (this.meepleDirection != null) {
				Direction t1MDirection = this.meepleDirection;
				Direction t2MDirection = tile.getMeepleDirection();
				for (int i = 0; i < 4; i++) {
					if (Utils.BASE_DIRECTIONS[i] == t1MDirection) {
//						System.out.println("t1 direction is " + t1MDirection + " and t2 direction is " + t2MDirection
//								+ " and rotation is " + rotations + " which would generate"
//								+ Utils.BASE_DIRECTIONS[(i + rotations) % 4]);
						if (Utils.BASE_DIRECTIONS[(i + rotations) % 4] == t2MDirection) {
							return true;
						} else
							return false;
					}
				}
			}
			else return true;
		}
		return false;
	}

}
