package se.miun.pebe1700.dt133g.mcts.ph;

import java.util.ArrayList;
import java.util.List;

import se.miun.pebe1700.dt133g.carcassonne.TerrainTile;
import se.miun.pebe1700.dt133g.carcassonne.Tile;
import se.miun.pebe1700.dt133g.carcassonne.Utils;
import se.miun.pebe1700.dt133g.utils.Point;

public class HistoryMap {

	private List<PlayedTile> tiles;
	private List<ScoreVisits> scoreVisits;

	public HistoryMap() {
		tiles = new ArrayList<>();
		scoreVisits = new ArrayList<>();
	}

	public PlayedTile addIncrementAndReturn(PlayedTile pt) {
		for (int i = 0; i < tiles.size(); i++) {
			if(this.sameTiles(tiles.get(i), pt)) {
				//System.out.println("found doublet, historymap size: " + tiles.size());
				scoreVisits.get(i).incrementVisits(1);
				return tiles.get(i);
			}
		}
//		System.out.println("NOT doublet, historymap size: " + tiles.size());
		
		tiles.add(pt);
		scoreVisits.add(new ScoreVisits());
		scoreVisits.get(scoreVisits.size() - 1).incrementVisits(1);
		return tiles.get(tiles.size() - 1);
	}

	public void incrementScoreForAffected(PlayedTile pt, double score) {
		for (int i = 0; i < tiles.size(); i++) {
			if(tiles.get(i)==pt) {
				//System.out.println("FATATA00");
				scoreVisits.get(i).incrementScore(score);
				break;
			}

		}
//		for (int i = 0; i < tiles.size(); i++) {
//
//			System.out.println("tile: " + tiles.get(i).getPartialStructures().toString() + "has "
//				+ scoreVisits.get(i).getScore() + " and " + scoreVisits.get(i).getVisits());
//
//		}
//
//		System.out.println();
//		System.out.println();
//		System.out.println();
//		System.out.println();
//		System.out.println();
//		System.out.println();
//		System.out.println();
//		System.out.println();
//		System.out.println();
//		System.out.println();
//		System.out.println(tiles.size() + " and " + scoreVisits.size());
//		System.out.println();
//		System.out.println();
//		System.out.println();
//		System.out.println();
//		System.out.println();
//		System.out.println();
//		System.out.println();
	}

	public ScoreVisits getScoreVisitFromTile(PlayedTile pt) {

		for (int i = 0; i < tiles.size(); i++) {
			if(this.sameTiles(tiles.get(i), pt)) {
				//System.out.println("SAAAAME with " + scoreVisits.get(i).getScore() + " and " + scoreVisits.get(i).getVisits());
				return scoreVisits.get(i); }
		}
		return null;
	}
	
	public boolean sameTiles(PlayedTile pt1, PlayedTile pt2) {
	
		if(pt1.getPlayer().getColour() != pt2.getPlayer().getColour()) {
			return false;
		}
		if(!pt1.getTile().sameTileUnregardPositionAndRotation(pt2.getTile())) {
			return false;
		}
		
		List<Tile> tilesN1 = new ArrayList<>();
		List<Tile> tilesN2 = new ArrayList<>();
		
		for(int i = 0; i < 4; i++) {
			tilesN1.add(pt1.getTile().getNeighbourInDirecton(Utils.BASE_DIRECTIONS[i]));
		}
		for(int i = 0; i < 4; i++) {
			tilesN2.add(pt2.getTile().getNeighbourInDirecton(Utils.BASE_DIRECTIONS[i]));
		}
		
		int nominator = 0;
		
		for(int i = 0; i < 4; i++) {
			for(int j = 0; j < 4; j++) {
				if(tilesN1.get(i) instanceof TerrainTile) {
					if(tilesN2.get(j) instanceof TerrainTile) {
						TerrainTile t1 = (TerrainTile) tilesN1.get(i);
						TerrainTile t2 = (TerrainTile) tilesN2.get(j);
						if(t1.sameTileUnregardPositionAndRotation(t2)) {
							nominator++;
						}
					}
				}
				else if(!(tilesN2.get(j) instanceof TerrainTile)) {
					nominator++;
				}
			}
		}
		
		return nominator >= 5;
		
		
		
//		return pt1.equals(pt2);
//		if(pt1.getGridSize() == pt2.getGridSize()) {
//			return(pt1.equals(pt2));
//		}
//		if(pt1.getGridSize() < pt2.getGridSize()) {
//			if(pt1.getTile().sameTileUnregardPosition(pt2.getTile())
//			&& pt1.getPlayer().getColour() == pt2.getPlayer().getColour()) {
//				Point p1 = new Point(pt1.getTile().getPosition());
//				Point p2 = new Point(pt2.getTile().getPosition());
//				p1.increment(pt2.getGridSize()-pt1.getGridSize());
//				return p1.equals(p2);
//			}
//		}
//		else {
//			if(pt1.getTile().sameTileUnregardPosition(pt2.getTile())
//				&& pt1.getPlayer().getColour() == pt2.getPlayer().getColour()) {
//					Point p1 = new Point(pt1.getTile().getPosition());
//					Point p2 = new Point(pt2.getTile().getPosition());
//					p2.increment(pt1.getGridSize()-pt2.getGridSize());
//					return p1.equals(p2);
//				}
//		}
//		return false;
	}

}
