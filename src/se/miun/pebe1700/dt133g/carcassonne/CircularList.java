package se.miun.pebe1700.dt133g.carcassonne;

import java.util.Arrays;
import java.util.Iterator;


public class CircularList<T> implements Iterable<T> {

	private T[] array;
	private int last, size;
	private final int MAX;

	@SuppressWarnings("unchecked")
	public CircularList(int max) {
		this.MAX = max;
		last = size = 0;
		array = (T[]) new Object[MAX];
	}

	public void add(T n) {
		if (!isFull()) {
			last %= MAX;
			array[last++] = n;
			size++;
		}
	}
	
	public T at(int n) {
		return array[n];
	}

	public boolean isFull() {
		return size == MAX;
	}

	public boolean isEmpty() {
		return size == 0;
	}
	
	public T getElementInClockwisePos(int k, int l) {
		return array[(k+l)%MAX];
	}
	
	public T getElementInCounterClockwisePos(int k, int l) {
		return array[Math.floorMod(k-l, MAX)];
	}

	public String toString() {

		String s = "";
		for (int i = 0; i < size; i++) {
			s += " " + array[i];
		}
		return s;
	}
	
	public int indexOf(T arg) {
		for(int i = 0 ; i < MAX ; i++) {
			if(arg.equals(array[i])) {
				return i;
			}
		}
		return -1;
	}
	
	
	public void leftShift(int k) {
		if (k == 0 || k == size) { return; }
		for (int i = 0; i < k; i++) {
			T headData = array[0];
			T tempData;

			for (int j = 0; j < size - 1; j++) {
				tempData = array[j+1];
				array[j] = tempData;
			}
			array[size-1] = headData;
		
		}
	}

	@Override
	public Iterator<T> iterator() {
		return Arrays.asList(array).iterator();
	}

}
