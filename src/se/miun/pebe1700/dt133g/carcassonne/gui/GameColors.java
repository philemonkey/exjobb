package se.miun.pebe1700.dt133g.carcassonne.gui;

import java.awt.Color;

public class GameColors {
	public static final Color FIELDS = Color.decode("#a3d14c");
	public static final Color CITY = Color.decode("#d4aa4d");
	public static final Color ROAD = Color.decode("#f6f8e1");
	public static final Color CLOISTER = Color.YELLOW;
	

	public static final String CELL_BACKGROUND = "#386551";
	public static final String CELL_OUTER_STROKE = "#21213d";
	public static final String OPP_CELL_OUTER_STROKE = "#3d2f21";
	
	
	

}
