package se.miun.pebe1700.dt133g.mcts.pb;

import se.miun.pebe1700.dt133g.carcassonne.GameBoard;
import se.miun.pebe1700.dt133g.carcassonne.Player;
import se.miun.pebe1700.dt133g.carcassonne.TerrainTile;
import se.miun.pebe1700.dt133g.carcassonne.Utils;
import se.miun.pebe1700.dt133g.carcassonne.enums.Feature;
import se.miun.pebe1700.dt133g.carcassonne.enums.Meeple;
import se.miun.pebe1700.dt133g.mcts.MCTSUtils;

public class Eval {

	public static double getEval(GameBoard gb, Player phasingPlayer, Player opposingPlayer,
		Meeple mainPlayerColour, int deckSize) {

		//double score = 0;

//		Player copyPhasing = new Player(phasingPlayer);
//		Player copyOpposing = new Player(opposingPlayer);

		Player mainPlayer = new Player(phasingPlayer.getColour() == mainPlayerColour ? phasingPlayer : opposingPlayer);
//		Player otherPlayer = new Player(phasingPlayer.getColour() == mainPlayerColour ? opposingPlayer : phasingPlayer);
//		score += countRemainingCloisters(gb) > 0 && mainPlayer.getRemainingMeeples() == 1 ? 1 : 0;
//		score += countRemainingCloisters(gb) > 0 && opposingPlayer.getRemainingMeeples() == 0 ? 1 : 0;
//		
//		score += mainPlayer.getScore() > opposingPlayer.getScore() ? 1 : 0;

//		Utils.calcFinalScore(new GameBoard(gb),
//			copyPhasing.getColour() == Meeple.RED ? copyPhasing : copyOpposing,
//			copyPhasing.getColour() == Meeple.BLUE ? copyPhasing : copyOpposing,
//			null);

//		System.out.println("red score before calc: " + red.getScore());
//		System.out.println("red score after calc: " + copyRed.getScore());
//		System.out.println("blue score before calc: " + blue.getScore());
//		System.out.println("blue score after calc: " + copyBlue.getScore());

//		return MCTSUtils.rewardPolicyWin(copyPhasing,
//			copyOpposing,
//			mainPlayerColour);

		return mainPlayer.getMeepleBroughtBack() * 2 + mainPlayer.getHighestScore();

	}

	public static int countRemainingCloisters(GameBoard gb) {
		int nCloisters = 6;
		for (TerrainTile t : gb.getTerrainTiles()) {
			if (t.getCenterFeature() == Feature.CLOISTER)
				nCloisters--;
		}
		return nCloisters;
	}

}
