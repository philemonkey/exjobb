package se.miun.pebe1700.dt133g.carcassonne.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.AdjustmentEvent;
import java.awt.event.AdjustmentListener;

import javax.swing.BorderFactory;
import javax.swing.JCheckBox;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import javax.swing.text.BadLocationException;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;

@SuppressWarnings("serial")
public class StatusPanel extends JPanel {

	private static JTextPane messageArea;
	private JScrollPane scroll;
	private JPanel optionsArea;
	private static Style style;
	private JCheckBox check;
	private AdjustmentListener autoScroll;

	public StatusPanel() {
		setLayout(new BorderLayout());
		
		optionsArea = new JPanel();
		optionsArea.setBackground(Color.LIGHT_GRAY);
		check = new JCheckBox("autoscroll", true);
		check.setBackground(Color.LIGHT_GRAY);
		messageArea = new JTextPane();
		messageArea.setBackground(Color.BLACK);
		messageArea.setFocusable(false);
		messageArea.setFont(new Font(Font.SANS_SERIF, 1, 20));
		style = messageArea.addStyle("the style",
			null);

		Border etched = BorderFactory.createEtchedBorder(EtchedBorder.RAISED);
		messageArea.setBorder(etched);
		messageArea.setEditable(false);
		scroll = new JScrollPane(messageArea);
		scroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		
		autoScroll = new AdjustmentListener(){
			 
			@Override
			 
			public void adjustmentValueChanged(AdjustmentEvent e) {
				e.getAdjustable().setValue(e.getAdjustable().getMaximum());
			}
		};
		
		enableAutoScroll();
		
		check.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
               if(check.isSelected()) {
            	   enableAutoScroll();
               }
               else {
            	   disableAutoScroll();
               }
            }
        });
		
		
		
		
		
		optionsArea.setPreferredSize(new Dimension(getWidth(), 50));
		
		optionsArea.add(check, BorderLayout.WEST);
		
		add(optionsArea,
			BorderLayout.NORTH);


		add(scroll,
			BorderLayout.CENTER);

	}
	
	
	

	public static void showNewMessage(String message, Color c) {

		StyleConstants.setForeground(style,
			c);
		try {
			appendString(message + "\n",
				style);
		} catch (BadLocationException e) {
			e.printStackTrace();
		}
	}
	
	private void enableAutoScroll() {
		scroll.getVerticalScrollBar().addAdjustmentListener(autoScroll);
	}
		
	private void disableAutoScroll() {
		scroll.getVerticalScrollBar().removeAdjustmentListener(autoScroll);
	}

	private static void appendString(String str, Style theStyle) throws BadLocationException {

		StyledDocument document = (StyledDocument) messageArea.getDocument();
		document.insertString(document.getLength(),
			str,
			theStyle);
	}

}
