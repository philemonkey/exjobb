package se.miun.pebe1700.dt133g.mcts;

public enum RewardPolicy {
WIN, SCORE, SCORE_AND_WIN, SCORE_BOTH
}
