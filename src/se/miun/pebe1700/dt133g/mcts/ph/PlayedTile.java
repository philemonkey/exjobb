package se.miun.pebe1700.dt133g.mcts.ph;

import se.miun.pebe1700.dt133g.carcassonne.Player;
import se.miun.pebe1700.dt133g.carcassonne.TerrainTile;
import se.miun.pebe1700.dt133g.carcassonne.Tile;
import se.miun.pebe1700.dt133g.carcassonne.Utils;
import se.miun.pebe1700.dt133g.utils.Point;

public class PlayedTile {

	private TerrainTile tile;
	private Player player;
	private final int TURN;

	public PlayedTile(TerrainTile tile, Player player, int turn) {
		this.TURN = turn;
		this.tile = tile;
		this.player = player;
		
	}

	public TerrainTile getTile() {
		return this.tile;
	}

	public Player getPlayer() {
		return this.player;
	}
	
	public int getTurn() {
		return TURN;
	}
 
	@Override
	public boolean equals(Object o) {
		PlayedTile pt = (PlayedTile) o;
		
		
		
		
		return (pt.getTile().sameTileUnregardPositionAndRotation(tile)
			&& pt.getPlayer().getColour() == player.getColour());
		
//		return (pt.getTile().sameTileUnregardPosition(tile)
//				&& pt.getPlayer().getColour() == player.getColour() && pt.getTurn() == TURN);
	}

}
