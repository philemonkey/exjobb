package se.miun.pebe1700.dt133g.carcassonne;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import se.miun.pebe1700.dt133g.carcassonne.enums.Direction;
import se.miun.pebe1700.dt133g.carcassonne.enums.Feature;
import se.miun.pebe1700.dt133g.carcassonne.enums.Meeple;

public class PartialStructure {

	private List<Direction> directions;
	private Feature feature;
	private Meeple meeple;
	private Direction meepleDirection;
	public PartialStructure() {
		directions = new ArrayList<>();
		meeple = null;
	}
	
	public void addDirection(Direction dir) {
		directions.add(dir);
	}
	
	public void addFeature(Feature feature) {
		this.feature = feature;
	}
	
	public List<Direction> getDirections() {
		return Collections.unmodifiableList(directions);
	}
	
	public void leftShiftDirections(int k) {
		for(int i = 0 ; i < directions.size(); i++) {
			for(int j = 0; j < 4 ; j++) {
				if(directions.get(i) == Utils.BASE_DIRECTIONS[j]) {
					directions.set(i, Utils.BASE_DIRECTIONS[Math.floorMod(j-k, 4)]);
					break;
				}
			}
		}
	}
	
	public Feature getFeature() {
		return this.feature;
	}
	
	public String toString() {
		return "[" + this.feature + ": " + directions.toString() + "]";	
	}
	
	public void setMeeple(Meeple meeple, Direction dir) {
		this.meeple = meeple;
		this.meepleDirection = dir;
	}
	
	public Meeple getMeeple() {
		return this.meeple;
	}
	
	public Direction getMeepleDirection() {
		return this.meepleDirection;
	}
	
	public void removeMeeple() {
		this.meeple = null;
		this.meepleDirection = null;
	}
	
}
