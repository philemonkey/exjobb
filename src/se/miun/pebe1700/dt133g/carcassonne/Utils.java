package se.miun.pebe1700.dt133g.carcassonne;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;

import com.opencsv.CSVWriter;

import se.miun.pebe1700.dt133g.carcassonne.enums.Direction;
import se.miun.pebe1700.dt133g.carcassonne.enums.Feature;
import se.miun.pebe1700.dt133g.carcassonne.enums.Meeple;
import se.miun.pebe1700.dt133g.carcassonne.logger.GameListener;
import se.miun.pebe1700.dt133g.utils.Point;
import se.miun.pebe1700.dt133g.utils.StackPushPullException;

public class Utils {

	public static final Direction[] BASE_DIRECTIONS = { Direction.NORTH, Direction.EAST,
			Direction.SOUTH, Direction.WEST };

	public static final Map<Direction, Direction> OPPOSITE_DIRECTION;
	static {
		Map<Direction, Direction> aMap = new HashMap<>();
		aMap.put(Direction.NORTH,
			Direction.SOUTH);
		aMap.put(Direction.EAST,
			Direction.WEST);
		aMap.put(Direction.SOUTH,
			Direction.NORTH);
		aMap.put(Direction.WEST,
			Direction.EAST);
		OPPOSITE_DIRECTION = Collections.unmodifiableMap(aMap);
	}

	public static LinkedList<TerrainTile> getOrderedDeck()
		throws NumberFormatException, IOException {
		LinkedList<TerrainTile> allTiles = new LinkedList<>();
		CircularList<Feature> outer;
		List<PartialStructure> partStructures;
		Feature inner;

		String row;
		BufferedReader csvReader = new BufferedReader(new FileReader("no_cloisters.csv"));
		while ((row = csvReader.readLine()) != null) {
			String[] data = row.split(",");
			for (int n = 0; n < Integer.parseInt(data[0]); n++) {
				outer = new CircularList<Feature>(4);
				partStructures = new ArrayList<>();
				for (int i = 1; i < 5; i++) {
					outer.add(Feature.valueOf(Feature.class,
						data[i]));
				}
				inner = Feature.valueOf(Feature.class,
					data[5]);

				int pos = data.length > 7 ? 7 : -1;
				for (int i = 0; i < Integer.parseInt(data[6]); i++) {
					PartialStructure part = new PartialStructure();
					part.addFeature(Feature.valueOf(Feature.class,
						data[pos++]));
					int nDirections = Integer.parseInt(data[pos++]);
					int start = pos;
					for (int j = start; j < start + nDirections; j++) {

						part.addDirection(Direction.valueOf(Direction.class,
							data[j]));
						pos++;
					}

					partStructures.add(part);
				}
				TerrainTile tile = new TerrainTile(outer, inner, partStructures);
				allTiles.add(tile);
			}

		}

		csvReader.close();
		return allTiles;
	}

	public static LinkedList<TerrainTile> getShuffledDeck()
		throws NumberFormatException, IOException {

		LinkedList<TerrainTile> allTiles = getOrderedDeck();

		Collections.shuffle(allTiles);
		return allTiles;
	}

	public static List<TerrainTile> CalculateTilePlacements(GameBoard board,
		final TerrainTile tile) {
		List<TerrainTile> placeableTiles = new ArrayList<>();

		// EACH placeable tile
		for (EmptyTile placeable : board.getPlaceableTiles()) {

			TerrainTile tempTile = new TerrainTile(tile);

			// EACH rotation of brick
			for (int rotation = 0; rotation < 4; rotation++) {
				boolean match = true;
				Map<Direction, Feature> placeableNeighbouringFeatures = placeable
					.getOuterFeatureInEachDirection();

				for (int j = 0; j < 4; j++) {

					if (placeableNeighbouringFeatures.containsKey(BASE_DIRECTIONS[j])) {

						if (tempTile.getFeatureFromDirection(
							BASE_DIRECTIONS[j]) != placeableNeighbouringFeatures
								.get(BASE_DIRECTIONS[j])) {
							match = false;
							break;
						}
					}
				}
				if (match) {
					TerrainTile placeableTile = new TerrainTile(tile);
					placeableTile.rotateTile(rotation);
					placeableTile.setPosition(placeable.getPosition());
					placeableTiles.add(placeableTile);
				}
				tempTile.rotateTile(1);
			}
		}
		return placeableTiles;
	}

	public static TerrainTile getRandomTile(List<TerrainTile> allTilesPlacements) {
		Random r = new Random();
		int random = r.nextInt(allTilesPlacements.size());
		return allTilesPlacements.get(random);
	}

	public static LinkedList<TerrainTile> copyDeck(LinkedList<TerrainTile> source) {
		LinkedList<TerrainTile> copied = new LinkedList<>();
		for (TerrainTile t : source) {
			copied.add(t);
		}
		return copied;
	}

	public static void evaluateStructure(TerrainTile tile, PartialStructure ps,
		List<TerrainTile> checkedTile, Structure data) {

		if (checkedTile.contains(tile)) {
			return;
		} else {
			// Add tile to structure and increment sum.
			checkedTile.add(tile);
			// System.out.println(tile.getPosition().toString());
			data.addToSize(1);
			data.addPartialStructure(ps);
			if (ps.getMeeple() != null) { data.addMeeple(ps.getMeeple()); }

		}

		// Every direction that structure is involved with
		// the feature in each "dir" is in fact a city space belonging to the
		// structure
		for (Direction dir : ps.getDirections()) {
			// A neighbour tile. Example West City feautre will look at the
			// neighbour at West.
			Tile t = tile.getNeighbourInDirecton(dir);

			// If the city is neigbouring an empty space
			if (t instanceof TerrainTile == false) {
				data.setStructureDone(false);
			} else {
				// else it must be a city space
				TerrainTile nTerrainTile = (TerrainTile) t;

				for (PartialStructure partial : nTerrainTile.getPartialStructures()) {
					for (Direction pDir : partial.getDirections()) {
						if (Utils.OPPOSITE_DIRECTION.get(dir) == pDir) {
							// System.out.println("will evaluate neighbour in "
							// + dir);
							evaluateStructure(nTerrainTile,
								partial,
								checkedTile,
								data);
						}
					}
				}
			}
		}

		return;
	}

	public static List<Point> getCloisterInProxy(GameBoard gb, Point position) {
		int row = position.y();
		int col = position.x();
		List<Point> cloisters = new ArrayList<>();
		for (int r = row - 1; r <= row + 1; r++) {
			for (int c = col - 1; c <= col + 1; c++) {
				if (gb.at(r,
					c) instanceof TerrainTile) {
					TerrainTile tile = (TerrainTile) gb.at(r,
						c);
					if (tile.getCenterFeature() == Feature.CLOISTER) {
						cloisters.add(tile.getPosition());
					}
				}
			}
		}
		return cloisters;
	}

	public static int getCloisterSize(GameBoard gb, Point position) {
		int row = position.y();
		int col = position.x();
		int size = 0;
		for (int r = row - 1; r <= row + 1; r++) {
			for (int c = col - 1; c <= col + 1; c++) {
				if (gb.at(r,
					c) instanceof TerrainTile) { size++; }
			}
		}
		return size;
	}

	public static List<Direction> calcPlaceableMeeples(GameBoard board, TerrainTile tile) {

		List<Direction> placeableMeeples = new ArrayList<>();

		for (PartialStructure ps : tile.getPartialStructures()) {

			if (ps.getFeature() != Feature.CLOISTER) {
				Structure data = new Structure(ps.getFeature());
				List<TerrainTile> checkedTile = new ArrayList<>();
				Utils.evaluateStructure(tile,
					ps,
					checkedTile,
					data);
				if (data.getMeeples().size() == 0) {
					placeableMeeples.add(ps.getDirections().get(0));
				}
			} else {
				// It's a cloister.
				placeableMeeples.add(Direction.CENTER);
			}

		}

		return placeableMeeples;
	}

	public static void pauseApp() {
		try {
			System.in.read();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void calcScore(GameBoard board, TerrainTile tile, Player red, Player blue,
		GameListener gameLog) {
		List<Point> cloisters = Utils.getCloisterInProxy(board,
			tile.getPosition());

		for (PartialStructure ps : tile.getPartialStructures()) {

			Structure data = new Structure(ps.getFeature());
			if (ps.getFeature() != Feature.CLOISTER) {
				List<TerrainTile> checkedTile = new ArrayList<>();
				Utils.evaluateStructure(tile,
					ps,
					checkedTile,
					data);
			}

			if (data.getFeature() != Feature.CLOISTER && data.getStructureDone()) {
				scoringManager(data,
					red,
					blue,
					gameLog);
			}

		}

		doCloisters(cloisters,
			board,
			red,
			blue);
	}

	// TODO Really shitty, but working
	public static void doCloisters(List<Point> cloisters, GameBoard board, Player red, Player blue) {
		for (Point p : cloisters) {
			if (Utils.getCloisterSize(board,
				p) == 9) {
				Meeple m = null;
				TerrainTile t = (TerrainTile) board.at(p);
				for (PartialStructure ps : t.getPartialStructures()) {
					if (ps.getFeature() == Feature.CLOISTER) {
						m = ps.getMeeple();
						ps.removeMeeple();

					}
				}
				if (m != null) {
					red.addScore(m == Meeple.RED ? 9 : 0);
					blue.addScore(m == Meeple.BLUE ? 9 : 0);
					if (m == Meeple.RED) {
						try {
							red.pushMeeple();
						} catch (StackPushPullException e) {
							e.printStackTrace();
						}
					} else {
						try {
							blue.pushMeeple();
						} catch (StackPushPullException e) {
							e.printStackTrace();
						}
					}
//					System.out.println("Cloister done and "
//							+ (m == Meeple.RED ? "RED scores" : "BLUE scores"));

					// TODO ta bort meeple från planen också.
				}
				// System.out.println("Cloister done");
			}
		}
	}

	// TODO Really shitty, but working
	public static void scoringManager(Structure data, Player red, Player blue,
		GameListener gameLog) {
		int score = data.getFeature() == Feature.CITY ? data.getSize() * 2 : data.getSize();
		if (data.getMeeples().size() > 1) {

			int[] redOrBlue = new int[2];
			for (Meeple m : data.getMeeples()) {
				if (m == Meeple.RED) {
					redOrBlue[0]++;
				} else {
					redOrBlue[1]++;
				}
			}
			if (redOrBlue[0] == redOrBlue[1]) {
				red.addScore(score);
				blue.addScore(score);
				if (gameLog != null)
					gameLog.structureDone("Both player scores for ",
						data);

			} else {
				red.addScore(redOrBlue[0] > redOrBlue[1] ? score : 0);
				blue.addScore(redOrBlue[0] < redOrBlue[1] ? score : 0);

				if (gameLog != null)
					gameLog.structureDone(
						(redOrBlue[0] > redOrBlue[1] ? "Red " : "Blue " + "scores ALONE for "),
						data);

			}

		} else if (data.getMeeples().size() == 1) {
			red.addScore(data.getMeeples().get(0) == Meeple.RED ? score : 0);
			blue.addScore(data.getMeeples().get(0) == Meeple.BLUE ? score : 0);
			if (gameLog != null)
				gameLog.structureDone(
					(data.getMeeples().get(0) == Meeple.RED ? "Red " : "Blue ") + "scores for ",
					data);

		} else {
			if (gameLog != null)
				gameLog.structureDone("No one scores for ",
					data);
		}

		List<Meeple> clearedMeeples = data.clearMeeples();
		for (Meeple meeple : clearedMeeples) {
			if (meeple == Meeple.RED) {
				try {
					red.pushMeeple();
				} catch (StackPushPullException e) {
					e.printStackTrace();
				}
			} else
				try {
					blue.pushMeeple();
				} catch (StackPushPullException e) {
					e.printStackTrace();
				}
		}

		if (gameLog != null)
			gameLog.playerStatus(red);
		if (gameLog != null)
			gameLog.playerStatus(blue);

	}

	public static void calcFinalScore(GameBoard gb, Player red, Player blue, GameListener gameLog) {
		for (TerrainTile tile : gb.getTerrainTiles()) {
			for (PartialStructure ps : tile.getPartialStructures()) {

				Structure data = new Structure(ps.getFeature());
				if (ps.getFeature() != Feature.CLOISTER) {
					List<TerrainTile> checkedTile = new ArrayList<>();
					Utils.evaluateStructure(tile,
						ps,
						checkedTile,
						data);
				} else if (ps.getFeature() == Feature.CLOISTER) {
					if (ps.getMeeple() != null) {
						doFinalCloister(gb,
							tile,
							ps.getMeeple(),
							red,
							blue,
							gameLog);
					}
				}

				if (data.getFeature() != Feature.CLOISTER) {
					finalScoringManager(data,
						red,
						blue,
						gameLog);
				}

			}
		}
	}

	public static void doFinalCloister(GameBoard gb, TerrainTile tile, Meeple m, Player red,
		Player blue, GameListener gameLog) {

		int score = getCloisterSize(gb,
			tile.getPosition());
		red.addScore(m == Meeple.RED ? score : 0);
		blue.addScore(m == Meeple.BLUE ? score : 0);
		if (m == Meeple.RED) {

			try {
				red.pushMeeple();
			} catch (StackPushPullException e) {
				e.printStackTrace();
			}

		} else {

			try {
				blue.pushMeeple();
			} catch (StackPushPullException e) {
				e.printStackTrace();
			}

		}
		if (gameLog != null) {
			gameLog.generalMessage("Final Score for cloister "
				+ (m == Meeple.RED ? "RED scores " : "BLUE scores ") + score);
		}

		tile.removeMeeple();
	}

	public static void finalScoringManager(Structure data, Player red, Player blue,
		GameListener gameLog) {
		int score = 1;
		if (data.getMeeples().size() > 1) {

			int[] redOrBlue = new int[2];
			for (Meeple m : data.getMeeples()) {
				if (m == Meeple.RED) {
					redOrBlue[0]++;
				} else {
					redOrBlue[1]++;
				}
			}
			if (redOrBlue[0] == redOrBlue[1]) {
				red.addScore(score);
				blue.addScore(score);
				if (gameLog != null)
					gameLog.structureDone("Both player scores for ",
						data);

			} else {
				red.addScore(redOrBlue[0] > redOrBlue[1] ? score : 0);
				blue.addScore(redOrBlue[0] < redOrBlue[1] ? score : 0);

				if (gameLog != null)
					gameLog.structureDone(
						(redOrBlue[0] > redOrBlue[1] ? "Red " : "Blue " + "scores ALONE for "),
						data);

			}

		} else if (data.getMeeples().size() == 1) {
			red.addScore(data.getMeeples().get(0) == Meeple.RED ? score : 0);
			blue.addScore(data.getMeeples().get(0) == Meeple.BLUE ? score : 0);
			if (gameLog != null)
				gameLog.structureDone(
					(data.getMeeples().get(0) == Meeple.RED ? "Red " : "Blue ") + "scores for ",
					data);

		} else {
			if (gameLog != null)
				gameLog.structureDone("No one scores for ",
					data);
		}

		List<Meeple> clearedMeeples = data.clearMeeples();
		for (Meeple meeple : clearedMeeples) {
			if (meeple == Meeple.RED) {

				try {
					red.pushMeeple();
				} catch (StackPushPullException e) {
					e.printStackTrace();
				}

			} else

				try {
					blue.pushMeeple();
				} catch (StackPushPullException e) {
					e.printStackTrace();
				}

		}

		if (gameLog != null)
			gameLog.playerStatus(red);
		if (gameLog != null)
			gameLog.playerStatus(blue);

	}

	public static void writeDataToCsv(String filePath, String[] data) {
		// first create file object for file placed at location
		// specified by filepath
		File file = new File(filePath);
		try {
			// create FileWriter object with file as parameter
			FileWriter outputfile = new FileWriter(file, true);

			// create CSVWriter object filewriter object as parameter
			CSVWriter writer = new CSVWriter(outputfile);

			writer.writeNext(data);

			// closing writer connection
			writer.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
