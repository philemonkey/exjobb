package se.miun.pebe1700.dt133g.mcts;

import java.util.concurrent.Callable;

import se.miun.pebe1700.dt133g.carcassonne.TerrainTile;

public class MCTSChunk implements Callable<TerrainTile> {

	MonteCarloTreeSearch mcts;

	public MCTSChunk(MonteCarloTreeSearch type) {
		this.mcts = type;
	}
	
	@Override
	public TerrainTile call() throws Exception {
		return mcts.getNextMove();
    } 
		
	
	


}
