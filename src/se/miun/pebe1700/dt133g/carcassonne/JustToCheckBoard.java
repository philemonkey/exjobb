package se.miun.pebe1700.dt133g.carcassonne;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import se.miun.pebe1700.dt133g.utils.Point;


public class JustToCheckBoard {

	private Tile[][] gameBoard;
	private int size;
	List<TerrainTile> terrainTiles;
	List<EmptyTile> placeableTiles;

	public JustToCheckBoard() {
		gameBoard = new Tile[9][9];
		size = 9;
		terrainTiles = new ArrayList<>();
		placeableTiles = new ArrayList<>();
		init();
	}
	

	private void init() {
		
		for(int row = 0; row < size ; row++) {
			for(int col = 0; col < size ; col++) {
				gameBoard[row][col] = new EmptyTile(false);
			}
		}
		

	}
	
	public void placeTile(Point p, TerrainTile tile) {
		this.placeTile(p.y(), p.x(), tile);
	}

	public void placeTile(int row, int col, TerrainTile tile) {
		placeableTiles.remove(gameBoard[row][col]);
		gameBoard[row][col] = tile;
		tile.setPosition(new Point(col,row));
		terrainTiles.add(tile);

	}
	
	public int getSize() {
		return this.size;
	}
	
	public boolean getOccupied(int row, int col) {
		return (gameBoard[row][col] instanceof TerrainTile);
	}
	
	public TerrainTile getTerrainTile(int row, int col) {
		return (TerrainTile) gameBoard[row][col];
	}
	
	public List<TerrainTile> getTerrainTiles() {
		return Collections.unmodifiableList(terrainTiles);
	}
	
	public List<EmptyTile> getPlaceableTiles() {
		return Collections.unmodifiableList(placeableTiles);
	}

	
	
	public Tile at(int row, int col) {
		return gameBoard[row][col];
	}
	
	
	public String toString() {
		String s = "";

		for (int row = 0; row < size; row++) {
			for (int col = 0; col < size; col++) {
				if(gameBoard[row][col] instanceof EmptyTile) {
					EmptyTile et = (EmptyTile) gameBoard[row][col];
					s+= et.getPlaceable() ? "P" : "-";
				}
				else {
					s += "T";
				}
				
			}
			s += "\n";
		}
		return s;
	}
	

}
