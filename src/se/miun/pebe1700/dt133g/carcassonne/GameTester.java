package se.miun.pebe1700.dt133g.carcassonne;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import se.miun.pebe1700.dt133g.carcassonne.agents.MCTSAgent;
import se.miun.pebe1700.dt133g.carcassonne.enums.Meeple;
import se.miun.pebe1700.dt133g.carcassonne.gui.AgentSetup;
import se.miun.pebe1700.dt133g.carcassonne.logger.GameListener;
import se.miun.pebe1700.dt133g.carcassonne.logger.GameLog;
import se.miun.pebe1700.dt133g.mcts.MonteCarloTreeSearch;
import se.miun.pebe1700.dt133g.mcts.RewardPolicy;
import se.miun.pebe1700.dt133g.mcts.VanillaMCTS;
import se.miun.pebe1700.dt133g.mcts.delayed_node_expansion.DelayedNodeExpansionMCTS;
import se.miun.pebe1700.dt133g.mcts.fpu.FirstPlayUrgencyMCTS;
import se.miun.pebe1700.dt133g.mcts.pb.ProgressiveBiasMCTS;
import se.miun.pebe1700.dt133g.mcts.pb_w_simcutoff.ProgressiveBiasSimCutoffMCTS;
import se.miun.pebe1700.dt133g.mcts.ph.ProgressiveHistoryMCTS;
import se.miun.pebe1700.dt133g.mcts.simcutoff.SimulationCutoffMCTS;
import se.miun.pebe1700.dt133g.mcts.simcutoff_v2.SimulationCutoffV2MCTS;
import se.miun.pebe1700.dt133g.utils.StackPushPullException;
import se.miun.pebe1700.dt133g.utils.Stopwatch;

public class GameTester {

	private GameBoard board;
	private LinkedList<TerrainTile> deck;
	private Player red;
	private Player blue;
	private Player currentPlayer;
	Stopwatch redTimer;
	Stopwatch blueTimer;
	private double totScoreRed;
	private double totScoreBlue;
	private double totWinRed;
	private double totWinBlue;
	private int nGames;
	private List<AgentSetup> agentSetup;

	//private GameListener gameLog;

	public GameTester(List<AgentSetup> agentSetup) {

		this.totScoreRed = 0;
		this.totScoreBlue = 0;
		this.totWinRed = 0;
		this.totWinBlue = 0;
		this.nGames = 0;
		this.agentSetup = agentSetup;

		for (AgentSetup as : agentSetup) {
			System.out.println(as.AGENT_TYPE + ", " + (as.ITERATIONS == 0 ? "Time Limit: " + as.TIME_LIMIT + "ms" : "Iterations: " + as.ITERATIONS)  + ", " + as.DETERMINIZATIONS
				+ ", " + as.THREADS + ", " + as.RANDOM_DECK + ", " + as.REWARD_POLICY);
		}
	}

	public void startGame() {
		//gameLog = new GameLog();
		red = new Player(Meeple.RED);
		blue = new Player(Meeple.BLUE);
		board = new GameBoard();
		redTimer = new Stopwatch();
		blueTimer = new Stopwatch();

		try {
			deck = Utils.getShuffledDeck();
		} catch (NumberFormatException | IOException e) {
			e.printStackTrace();
		}
		currentPlayer = red;

	}

	public boolean makeMoveTest() {
		if (!deck.isEmpty()) {

			Action action = null;
			TerrainTile tileFromDeck = null;
			boolean done = false;
			while (!done) {
				tileFromDeck = deck.poll();
				List<TerrainTile> actions = Utils.CalculateTilePlacements(board,
					tileFromDeck);
				if (!actions.isEmpty()) { done = true; }
			}

			if (currentPlayer == red) {
				redTimer.start();

				List<MonteCarloTreeSearch> agents = new ArrayList<>();
				for (int i = 0; i < agentSetup.get(0).DETERMINIZATIONS; i++) {
					LinkedList<TerrainTile> deckCopy = Utils.copyDeck(deck);
					if (agentSetup.get(0).RANDOM_DECK) { Collections.shuffle(deckCopy); }
					agents.add(initializer(red,
						blue,
						agentSetup.get(0),
						board,
						tileFromDeck,
						deckCopy));
				}

				MCTSAgent mcts = new MCTSAgent(agents, agentSetup.get(0).THREADS);
				action = mcts.getAction();
				redTimer.halt();
			} else {
				blueTimer.start();
				List<MonteCarloTreeSearch> agents = new ArrayList<>();
				for (int i = 0; i < agentSetup.get(1).DETERMINIZATIONS; i++) {
					LinkedList<TerrainTile> deckCopy = Utils.copyDeck(deck);
					if (agentSetup.get(1).RANDOM_DECK) { Collections.shuffle(deckCopy); }
					agents.add(initializer(blue,
						red,
						agentSetup.get(1),
						board,
						tileFromDeck,
						deckCopy));
				}

				MCTSAgent mcts = new MCTSAgent(agents, agentSetup.get(1).THREADS);
				action = mcts.getAction();
				blueTimer.halt();
			}

			board.placeTile(action.getTile().getPosition(),
				action.getTile());
//			gameLog.tilePlaced(currentPlayer,
//				action.getTile());

			// System.out.println(Utils.calcPlaceableMeeples(board, action.getTile()));

			if (action.getMeepleDirection() != null) {
				action.getTile()
					.placeMeeple(currentPlayer.getColour(),
						action.getMeepleDirection());

				try {
					currentPlayer.popMeeple();
				} catch (StackPushPullException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

//				gameLog.meeplePlaced(currentPlayer,
//					action.getTile(),
//					action.getTile().getFeatureFromDirection(action.getMeepleDirection()));
//				gameLog.playerStatus(red);
//				gameLog.playerStatus(blue);
			}

			Utils.calcScore(board,
				action.getTile(),
				red,
				blue,
				null);

//			System.out.println(deck.size() + " left");
			currentPlayer = currentPlayer == red ? blue : red;
			return true;
		}
		Utils.calcFinalScore(board,
			red,
			blue,
			null);

//		gameLog.finalScore(red,
//			blue);

		System.out.println("");
		if (red.getScore() == blue.getScore()) {
			System.out.println("draw");
		} else {
			System.out.println((red.getScore() > blue.getScore() ? "RED" : "BLUE") + " wins");
		}

		System.out.println("RED time: " + redTimer.getTotal(TimeUnit.SECONDS));
		System.out.println("BLUE time: " + blueTimer.getTotal(TimeUnit.SECONDS));
		System.out.println("RED score: " + red.getScore());
		System.out.println("BLUE score: " + blue.getScore());
		String[] data = {
				red.getScore() == blue.getScore() ? "DRAW"
					: ((red.getScore() > blue.getScore()) ? "RED" : "BLUE"),
				String.valueOf(red.getScore()), String.valueOf(redTimer.getTotal(TimeUnit.SECONDS)),
				String.valueOf(blue.getScore()),
				String.valueOf(blueTimer.getTotal(TimeUnit.SECONDS)) };

		Utils.writeDataToCsv("data/SC5u_vs_Vu_3000ms_01_01.csv", data);

		totScoreRed += red.getScore();
		totScoreBlue += blue.getScore();
		totWinRed += red.getScore() > blue.getScore() ? 1 : 0;
		totWinBlue += blue.getScore() > red.getScore() ? 1 : 0;
		nGames++;

		System.out.println("RED avg score: " + totScoreRed / (double) nGames);
		System.out.println("BLUE avg score: " + totScoreBlue / (double) nGames);
		System.out.println("RED avg win: " + totWinRed / (double) nGames);
		System.out.println("BLUE avg win: " + totWinBlue / (double) nGames);
		System.out.println("nGames: " + nGames);
		System.out.println();

		return false;
	}

	private MonteCarloTreeSearch initializer(Player phasingPlayer, Player opposingPlayer,
		AgentSetup agentSetup, GameBoard board, TerrainTile tileFromDeck,
		LinkedList<TerrainTile> deckCopy) {

		switch (agentSetup.AGENT_TYPE) {
		case "vanilla":
			return new VanillaMCTS(new GameBoard(board), tileFromDeck, deckCopy, phasingPlayer,
				opposingPlayer, agentSetup.ITERATIONS, agentSetup.TIME_LIMIT, agentSetup.REWARD_POLICY, agentSetup.REWARD_POLICY == RewardPolicy.WIN ? 6*1d/1.41d :0.1d);
		case "vanilla (delayed node expansion)":
			return new DelayedNodeExpansionMCTS(new GameBoard(board), tileFromDeck, deckCopy, phasingPlayer,
				opposingPlayer, agentSetup.ITERATIONS, agentSetup.TIME_LIMIT, agentSetup.REWARD_POLICY, agentSetup.REWARD_POLICY == RewardPolicy.WIN ? 6*1d/1.41d :0.1d);
		case "progressive history":
			return new ProgressiveHistoryMCTS(new GameBoard(board), tileFromDeck, deckCopy,
				phasingPlayer, opposingPlayer, agentSetup.ITERATIONS, agentSetup.TIME_LIMIT, agentSetup.REWARD_POLICY,
				0.1d);
		case "progressive bias":
			return new ProgressiveBiasMCTS(new GameBoard(board), tileFromDeck, deckCopy,
				phasingPlayer, opposingPlayer, agentSetup.ITERATIONS, agentSetup.TIME_LIMIT, agentSetup.REWARD_POLICY,
				0.1d);
		case "first play urgency":
			return new FirstPlayUrgencyMCTS(new GameBoard(board), tileFromDeck, deckCopy,
				phasingPlayer, opposingPlayer, agentSetup.ITERATIONS, agentSetup.TIME_LIMIT, agentSetup.REWARD_POLICY,
				0.1d);
		case "pb with simulation cutoff":
			return new ProgressiveBiasSimCutoffMCTS(new GameBoard(board), tileFromDeck, deckCopy,
				phasingPlayer, opposingPlayer, agentSetup.ITERATIONS, agentSetup.TIME_LIMIT, agentSetup.REWARD_POLICY,
				0.1d);
		default:
            if (agentSetup.AGENT_TYPE.length() > 17) {
            
            	if(agentSetup.AGENT_TYPE.substring(0, 17).equals("simulation cutoff")) {
            		if(agentSetup.AGENT_TYPE.substring(0, 18).equals("simulation cutoff2")) {
            			return new SimulationCutoffV2MCTS(new GameBoard(board), tileFromDeck, deckCopy,
                				phasingPlayer, opposingPlayer, agentSetup.ITERATIONS, agentSetup.TIME_LIMIT, agentSetup.REWARD_POLICY,
                				0.1d, Integer.parseInt(agentSetup.AGENT_TYPE.substring(19, agentSetup.AGENT_TYPE.length())));
            		}
            		else {
            			return new SimulationCutoffMCTS(new GameBoard(board), tileFromDeck, deckCopy,
                				phasingPlayer, opposingPlayer, agentSetup.ITERATIONS, agentSetup.TIME_LIMIT, agentSetup.REWARD_POLICY,
                				0.1d, Integer.parseInt(agentSetup.AGENT_TYPE.substring(18, agentSetup.AGENT_TYPE.length())));
            		}
            		
            	}
            }
            break;
		}
		
		return null;

	}

	public Meeple getCurrentPlayer() {
		return currentPlayer.getColour();
	}

	public GameBoard getBoard() {
		return this.board;
	}

}
