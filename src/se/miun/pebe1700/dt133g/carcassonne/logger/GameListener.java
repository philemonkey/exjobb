package se.miun.pebe1700.dt133g.carcassonne.logger;

import se.miun.pebe1700.dt133g.carcassonne.Player;
import se.miun.pebe1700.dt133g.carcassonne.Structure;
import se.miun.pebe1700.dt133g.carcassonne.TerrainTile;
import se.miun.pebe1700.dt133g.carcassonne.enums.Feature;

public interface GameListener {

	public void tilePlaced(Player player, TerrainTile tile);

	public void meeplePlaced(Player player, TerrainTile tile, Feature feature);
	
	public void structureDone(String mess, Structure structure);

	public void meepleScore(Player player, Structure structure);

	public void playerStatus(Player player);
	
	public void finalScore(Player player1, Player player2);
	
	public void generalMessage(String mess);

}