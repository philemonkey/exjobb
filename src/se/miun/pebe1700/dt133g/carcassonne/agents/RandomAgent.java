package se.miun.pebe1700.dt133g.carcassonne.agents;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import se.miun.pebe1700.dt133g.carcassonne.Action;
import se.miun.pebe1700.dt133g.carcassonne.GameBoard;
import se.miun.pebe1700.dt133g.carcassonne.PartialStructure;
import se.miun.pebe1700.dt133g.carcassonne.Player;
import se.miun.pebe1700.dt133g.carcassonne.Structure;
import se.miun.pebe1700.dt133g.carcassonne.TerrainTile;
import se.miun.pebe1700.dt133g.carcassonne.Utils;
import se.miun.pebe1700.dt133g.carcassonne.enums.Direction;
import se.miun.pebe1700.dt133g.carcassonne.enums.Feature;

public class RandomAgent extends Agent {


	private Direction getMeeplePlacement(GameBoard board, TerrainTile tile,
			Player player) {
		Direction meepleDirection = null;
	
		final double QUOTA = player.getRemainingMeeples()
				/ (player.MEEPLE_MAX * 2d);
		for (PartialStructure ps : tile.getPartialStructures()) {

			Structure data = new Structure(ps.getFeature());
			if (ps.getFeature() != Feature.CLOISTER) {
				List<TerrainTile> checkedTile = new ArrayList<>();
				Utils.evaluateStructure(tile, ps, checkedTile, data);
			}

			if (player.getRemainingMeeples() > 0
					&& data.getMeeples().size() == 0) {

				if (QUOTA > Math.random()) {
					meepleDirection = ps.getDirections().get(0);
				
					break;
				}
			}
		}

		return meepleDirection;
	}

	@Override
	public Action getAction(GameBoard board, TerrainTile tileFromDeck,
			LinkedList<TerrainTile> deck, Player phasingPlayer,
			Player opposingPlayer) {
		TerrainTile tile = new TerrainTile(tileFromDeck);

		tile = Utils.getRandomTile(Utils.CalculateTilePlacements(board, tile));
		
		board.placeTile(tile.getPosition(), tile);
		
		Direction dir = this.getMeeplePlacement(board, tile, phasingPlayer);
		
		if(board.boardJustExpanded) {
			tile.decrementPosition();
		}
		return new Action(tile, dir);
	}

	@Override
	public Action getAction() {
		return null;
	}
	
	

}
