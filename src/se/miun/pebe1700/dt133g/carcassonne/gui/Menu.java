package se.miun.pebe1700.dt133g.carcassonne.gui;

import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.KeyStroke;

import io.reactivex.Observable;
import se.miun.pebe1700.dt133g.mcts.RewardPolicy;

public class Menu {
	private static JMenuItem miNew;
	private static JMenuItem TEST;

	public static JMenuBar createMenuBar(MainFrame mainFrame) {
		JMenuBar menuBar;
		JMenu menu;
		JMenuItem menuItem;

		menuBar = new JMenuBar();

		menu = new JMenu("Start");

		menu.setMnemonic(KeyEvent.VK_S);
		menuBar.add(menu);

		miNew = new JMenuItem("New Game");
		miNew.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N, KeyEvent.CTRL_DOWN_MASK));
		miNew.addActionListener(e -> {
			List<AgentSetup> setup = gameSetup(mainFrame);
			if (setup != null) {
				mainFrame.initGame(setup);
			}

		});
		menu.add(miNew);

		TEST = new JMenuItem("TEST");
		TEST.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_C, KeyEvent.CTRL_DOWN_MASK));
		TEST.addActionListener(e -> {
			// mainFrame.getRandomBrick();

		});
		menu.add(TEST);

		menu.addSeparator();

		menuItem = new JMenuItem("Exit");
		menuItem.addActionListener(e -> System.exit(0));
		menu.add(menuItem);

		return menuBar;

	}

	private static List<AgentSetup> gameSetup(MainFrame mainFrame) {

		String[] type = new String[2];
		Integer[] iterations = new Integer[2];
		Integer[] timeLimit = new Integer[2];
		Integer[] nAgents = new Integer[2];
		Boolean[] randomDeck = new Boolean[2];
		RewardPolicy[] rewardPolicy = new RewardPolicy[2];
		Integer[] threads = new Integer[2];

		for (int i = 0; i < 2; i++) {
			final String[] player = new String[] { "RED", "BLUE" };
			final String[] mctsTypes = new String[] { "vanilla", "vanilla (delayed node expansion)", "progressive history", "progressive bias",
					"first play urgency", "simulation cutoff" , "simulation cutoff2", "pb with simulation cutoff" };
			type[i] = (String) JOptionPane.showInputDialog(mainFrame, "Select agent type for " + player[i], "Setup",
					JOptionPane.QUESTION_MESSAGE, null, mctsTypes, mctsTypes[0]);
			String choice = "abc";
			if (type[i] == null) {
				return null;
			} else if (type[i] == "simulation cutoff" ||  type[i] == "simulation cutoff2") {
				while (!parse(choice)) {
					choice = (String) JOptionPane.showInputDialog(mainFrame, "Sims until cutoff");
					if (choice == null) {
						return null;
					}
				}
				type[i] += " " + choice;

			}

			choice = "abc";
			while (!parse(choice)) {
				choice = (String) JOptionPane.showInputDialog(mainFrame, "(0 for time limit) Select number of iterations for " + player[i]);
				if (choice == null) {
					return null;
				}
			}
			iterations[i] = Integer.parseInt(choice);
			if (iterations[i] == 0) {
				choice = "abc";
				while (!parse(choice)) {
					choice = (String) JOptionPane.showInputDialog(mainFrame, "Select time limit (ms) for " + player[i]);
					if (choice == null) {
						return null;
					}
				}
				timeLimit[i] = Integer.parseInt(choice);
			} else {
				timeLimit[i] = 0;
			}
			choice = "abc";
			while (!parse(choice)) {
				choice = (String) JOptionPane.showInputDialog(mainFrame, "Select number of agents for " + player[i]);
				if (choice == null) {
					return null;
				}
			}
			nAgents[i] = Integer.parseInt(choice);

			String[] trueFalse = new String[] { "true", "false" };
			randomDeck[i] = Boolean.parseBoolean((String) JOptionPane.showInputDialog(mainFrame,
					"Select if incoming deck should be shuffled for " + player[i], "Setup",
					JOptionPane.QUESTION_MESSAGE, null, trueFalse, trueFalse[0]));

			if (randomDeck[i] == null) {
				return null;
			}

			String[] policies = new String[] { "SCORE", "WIN", "SCORE_AND_WIN", "SCORE_BOTH" };
			rewardPolicy[i] = Enum.valueOf(RewardPolicy.class,
					(String) JOptionPane.showInputDialog(mainFrame, "Select reward policy for " + player[i], "Setup",
							JOptionPane.QUESTION_MESSAGE, null, policies, policies[0]));

			if (rewardPolicy[i] == null) {
				return null;
			}

			threads[i] = (Integer) JOptionPane.showInputDialog(mainFrame, "Select number of threads for " + player[i],
					"Setup", JOptionPane.QUESTION_MESSAGE, null,
					Observable.range(1, 8).toList().blockingGet().toArray(), 1);

			if (threads[i] == null) {
				return null;
			}

		}

		List<AgentSetup> agentSetups = new ArrayList<>();
		agentSetups.add(new AgentSetup(iterations[0], timeLimit[0], nAgents[0], threads[0], randomDeck[0], type[0],
				rewardPolicy[0]));
		agentSetups.add(new AgentSetup(iterations[1], timeLimit[1], nAgents[1], threads[1], randomDeck[1], type[1],
				rewardPolicy[1]));

		return agentSetups;
	}

	private static boolean parse(String s) {
		try {
			Integer.parseInt(s);
			return true;
		} catch (NumberFormatException e) {
			return false;
		}
	}

}
