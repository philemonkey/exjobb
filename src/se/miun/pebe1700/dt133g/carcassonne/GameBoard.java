package se.miun.pebe1700.dt133g.carcassonne;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;

import se.miun.pebe1700.dt133g.carcassonne.enums.Direction;
import se.miun.pebe1700.dt133g.carcassonne.enums.Feature;
import se.miun.pebe1700.dt133g.carcassonne.enums.Meeple;
import se.miun.pebe1700.dt133g.utils.Point;


public class GameBoard {

	private Tile[][] gameBoard;
	private int size;
	List<TerrainTile> terrainTiles;
	List<EmptyTile> placeableTiles;
	public boolean boardJustExpanded = false;

	public GameBoard() {
		gameBoard = new Tile[5][5];
		size = 5;
		terrainTiles = new ArrayList<>();
		placeableTiles = new ArrayList<>();
		init();
	}
	
	
	public GameBoard(GameBoard boardToCopy) {
		size = boardToCopy.getSize();
		gameBoard = new Tile[size][size];
		
		terrainTiles = new ArrayList<>();
		placeableTiles = new ArrayList<>();
		initCopy(boardToCopy);
		this.boardJustExpanded = boardToCopy.boardJustExpanded;
	}
	
	public void placeMeeple(Meeple meeple, Direction dir, Point p) {
		TerrainTile tile = (TerrainTile) this.at(p);
		tile.placeMeeple(meeple, dir);
		
	}
	
	private void initCopy(GameBoard boardToCopy) {
		
		// TODO everything in one loop?
		for(int row = 0; row < size ; row++) {
			for(int col = 0; col < size ; col++) {
				gameBoard[row][col] = new EmptyTile(false);
			}
		}
		for(int row = 0; row < size ; row++) {
			for(int col = 0; col < size ; col++) {
				if(boardToCopy.at(row, col) instanceof TerrainTile) {
					TerrainTile tile = new TerrainTile((TerrainTile) boardToCopy.at(row, col));
					this.placeTile(row, col, tile);
				}
			}
		}
		
	}
	

	private void init() {
		
		for(int row = 0; row < size ; row++) {
			for(int col = 0; col < size ; col++) {
				gameBoard[row][col] = new EmptyTile(false);
			}
		}
		CircularList<Feature> outer = new CircularList<Feature>(4);
		// Adds data to the list
		outer.add(Feature.CITY);
		outer.add(Feature.ROAD);
		outer.add(Feature.FIELDS);
		outer.add(Feature.ROAD);
		
		List<PartialStructure> partStructures = new ArrayList<>();
		
		PartialStructure part = new PartialStructure();
		part.addFeature(Feature.CITY);
		part.addDirection(Direction.NORTH);
		partStructures.add(part);
		
		part = new PartialStructure();
		part.addFeature(Feature.ROAD);
		part.addDirection(Direction.EAST);
		part.addDirection(Direction.WEST);
		partStructures.add(part);
		
		TerrainTile tile = new TerrainTile(outer, Feature.NONE, partStructures);
		
		
		
		placeTile(2,2, tile);
	}
	
	public void placeTile(Point p, TerrainTile tile) {
		this.placeTile(p.y(), p.x(), tile);
	}

	public void placeTile(int row, int col, TerrainTile tile) {
		removePlaceable(new Point(col, row));
		gameBoard[row][col] = tile;
		tile.setPosition(new Point(col,row));
		terrainTiles.add(tile);
	
		boolean expanded = false;
		if (row == 1 || col == 1 || row == size - 2 || col == size - 2) {
			expanded = true;
			expandBoard();
			boardJustExpanded = true;
		}
		else {
			boardJustExpanded = false;
		}
		updateNeighbours(expanded ? row+1 : row, expanded ? col +1 : col, tile);

	}
	
	public void removePlaceable(Point p) {
		for(int i = 0; i < placeableTiles.size(); i++) {
			if(placeableTiles.get(i).getPosition().equals(p)) {
				placeableTiles.remove(i);

			}
		}
	}
	
	public int getSize() {
		return this.size;
	}
	
	public boolean getOccupied(int row, int col) {
		return (gameBoard[row][col] instanceof TerrainTile);
	}
	
	public TerrainTile getTerrainTile(int row, int col) {
		return (TerrainTile) gameBoard[row][col];
	}
	
	public List<TerrainTile> getTerrainTiles() {
		return Collections.unmodifiableList(terrainTiles);
	}
	
	public List<EmptyTile> getPlaceableTiles() {
		return Collections.unmodifiableList(placeableTiles);
	}

	private void expandBoard() {
		size = size +2;
		Tile[][] tempBoard = new Tile[size][size];
		
		for(int col = 0; col < size ; col++) {
			tempBoard[0][col] = new EmptyTile(false);
		}
		
		for(int col = 0; col < size ; col++) {
			tempBoard[size-1][col] = new EmptyTile(false);
		}
		
		for(int row = 1; row < size-1 ; row++) {
			tempBoard[row][0] = new EmptyTile(false);
			tempBoard[row][size-1] = new EmptyTile(false);
		}
		
		for(int row = 1; row < size -1 ; row++) {
			for(int col = 1; col < size-1 ; col++) {
				tempBoard[row][col] = gameBoard[row-1][col-1];
			}
		}
		gameBoard = tempBoard;
		recalcTilePosition();
		
	}


	public void updateNeighbours(int row, int col, TerrainTile tile) {

		tile.setNeighbour(Direction.NORTH, gameBoard[row - 1][col]);
		tile.setNeighbour(Direction.EAST, gameBoard[row][col + 1]);
		tile.setNeighbour(Direction.SOUTH, gameBoard[row + 1][col]);
		tile.setNeighbour(Direction.WEST, gameBoard[row][col - 1]);

		setNeighbour(row-1, col, tile, Direction.SOUTH);
		setNeighbour(row, col+1, tile, Direction.WEST);
		setNeighbour(row+1, col, tile, Direction.NORTH);
		setNeighbour(row, col-1, tile, Direction.EAST);
		
		
		
		LinkedHashSet<EmptyTile> hashSet = new LinkedHashSet<>(placeableTiles);
        placeableTiles = new ArrayList<>(hashSet);
		

	}
	
	public void setNeighbour(int row, int col, TerrainTile tile, Direction dir) {
		if (gameBoard[row][col] instanceof TerrainTile) {
			TerrainTile neighbour = (TerrainTile) gameBoard[row][col];
			neighbour.setNeighbour(dir, tile);
		}
		else {
			
			EmptyTile neighbour = (EmptyTile) gameBoard[row][col];
			neighbour.setPlaceable(true);
			neighbour.setPosition(new Point(col,row));
			neighbour.setNeighbour(Direction.NORTH, gameBoard[row - 1][col]);
			neighbour.setNeighbour(Direction.EAST, gameBoard[row][col + 1]);
			neighbour.setNeighbour(Direction.SOUTH, gameBoard[row + 1][col]);
			neighbour.setNeighbour(Direction.WEST, gameBoard[row][col - 1]);
			placeableTiles.add(neighbour);
		}
	}
	
	private void recalcTilePosition() {
		for(TerrainTile t: terrainTiles) {
			t.incrementPosition();
		}
		for(EmptyTile t: placeableTiles) {
			t.incrementPosition();
		}
	}
	
	public Tile at(int row, int col) {
		return gameBoard[row][col];
	}
	
	public Tile at(Point p) {
		return gameBoard[p.y()][p.x()];
	}
	
	
	public String toString() {
		String s = "";

		for (int row = 0; row < size; row++) {
			for (int col = 0; col < size; col++) {
				if(gameBoard[row][col] instanceof EmptyTile) {
					EmptyTile et = (EmptyTile) gameBoard[row][col];
					s+= et.getPlaceable() ? "P" : "-";
				}
				else {
					s += "T";
				}
				
			}
			s += "\n";
		}
		return s;
	}
	

}
