package se.miun.pebe1700.dt133g.carcassonne.gui;

import javax.swing.SwingUtilities;



public class AppStart {

	public static void main(String[] args) {
		
		
		// Make sure GUI is created on the event dispatching thread
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				new MainFrame().setVisible(true);
			}
		});
	}
}