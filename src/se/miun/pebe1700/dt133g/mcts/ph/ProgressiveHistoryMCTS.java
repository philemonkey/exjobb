package se.miun.pebe1700.dt133g.mcts.ph;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import se.miun.pebe1700.dt133g.carcassonne.Action;
import se.miun.pebe1700.dt133g.carcassonne.GameBoard;
import se.miun.pebe1700.dt133g.carcassonne.Player;
import se.miun.pebe1700.dt133g.carcassonne.TerrainTile;
import se.miun.pebe1700.dt133g.carcassonne.Utils;
import se.miun.pebe1700.dt133g.carcassonne.agents.RandomAgent;
import se.miun.pebe1700.dt133g.carcassonne.enums.Direction;
import se.miun.pebe1700.dt133g.carcassonne.enums.Meeple;
import se.miun.pebe1700.dt133g.mcts.GameState;
import se.miun.pebe1700.dt133g.mcts.MCTSUtils;
import se.miun.pebe1700.dt133g.mcts.MonteCarloTreeSearch;
import se.miun.pebe1700.dt133g.mcts.Node;
import se.miun.pebe1700.dt133g.mcts.RewardPolicy;
import se.miun.pebe1700.dt133g.utils.StackPushPullException;
import se.miun.pebe1700.dt133g.utils.Stopwatch;

public class ProgressiveHistoryMCTS extends MonteCarloTreeSearch {

	private HistoryMap historyMap;

	private int a;
	public final int CUTOFF;

	public ProgressiveHistoryMCTS(GameBoard board, TerrainTile tileFromDeck,
		LinkedList<TerrainTile> deck, Player mainPlayer, Player otherPlayer, int nIterations,int timeLimit,
		RewardPolicy rewardPolicy, double UCTconstant) {
		super(board, tileFromDeck, deck, mainPlayer, otherPlayer, nIterations, timeLimit, rewardPolicy,
			UCTconstant);
		CUTOFF = nIterations/3;

		a = 0;
		historyMap = new HistoryMap();
		expandRoot();
	}

	@Override
	public TerrainTile getNextMove() {
		
		int limit = -1;
		if(N_ITERATIONS == 0) {
			limit = TIME_LIMIT;
			stopwatch = new Stopwatch();
			stopwatch.start();
		}
		else {
			limit = N_ITERATIONS;
		}
		
		int a = 0;
		
		while (a < limit) {
			Node leaf = traverse(root);
			if (leaf != null) {
				Double simulationResult = rollout(new GameBoard(leaf.getGameBoard()),
						Utils.copyDeck(leaf.getRemainingDeck()),
						new Player(leaf.getOpposingPlayer()),
						new Player(leaf.getPhasingPlayer()), leaf.getState().getTurn());
					backPropagate(leaf,
						simulationResult);
			}

			if(N_ITERATIONS == 0) {
				stopwatch.stop();
				a = (int) stopwatch.getTime(TimeUnit.MILLISECONDS);
			}
			else {
				a++;
			}
			
		}

//		for (Node n : root.getChildNodes()) {
//			System.out.println("nVisited = " + n.getVisitCount() + ", Score = " + n.getTotalScore()
//				+ ", Action = " + n.getAction().getPosition().toString() + " "
//				+ n.getAction().getMeepleDirection());
//		}
//		System.out.println();
//		System.out.println();
//		System.out.println();
//		System.out.println();
		Node best = bestChild(root);

		if (best.getGameBoard().boardJustExpanded) {
			// System.out.println("this happened");
			best.getAction().decrementPosition();
		}
		return best.getAction();
	}

	private Node bestChild(Node root) {
		return root.getChildNodes()
			.stream()
			.max(Comparator.comparing(Node::getTotalScore))
			.orElseThrow(NoSuchElementException::new);
	}

	private Node traverse(Node root) {
		Node promisingNode = selectPromisingNode(root);

		Node rolloutNode = expandOne(promisingNode);

		return rolloutNode;
	}

	private void expandRoot() {

		LinkedList<TerrainTile> deckCopy = Utils.copyDeck(root.getRemainingDeck());
		TerrainTile tile = tileFromDeck;

		List<TerrainTile> possibleTilePlacements = Utils.CalculateTilePlacements(
			(GameBoard) root.getGameBoard(),
			tile);

		while (deckCopy.size() > 0 && possibleTilePlacements.size() == 0) {
			tile = new TerrainTile(deckCopy.poll());
		}
		Collections.shuffle(possibleTilePlacements);

		possibleTilePlacements.forEach(tilePlacement -> {
			GameBoard tempBoard = new GameBoard(root.getGameBoard());
			tempBoard.placeTile(tilePlacement.getPosition(),
				tilePlacement);

			if (root.getOpposingPlayer().getRemainingMeeples() > 0) {

				for (Direction dir : Utils.calcPlaceableMeeples(tempBoard,
					tilePlacement)) {

					Player phasingPlayer = new Player(root.getOpposingPlayer());
					Player opposingPlayer = new Player(root.getPhasingPlayer());
					GameBoard finalBoard = new GameBoard(tempBoard);

					finalBoard.placeMeeple(root.getOpposingPlayer().getColour(),
						dir,
						tilePlacement.getPosition());

					try {
						phasingPlayer.popMeeple();
					} catch (StackPushPullException e) {
						e.printStackTrace();
					}
					Utils.calcScore(finalBoard,
						(TerrainTile) finalBoard.at(tilePlacement.getPosition()),
						phasingPlayer.getColour() == Meeple.RED ? phasingPlayer : opposingPlayer,
						phasingPlayer.getColour() == Meeple.BLUE ? phasingPlayer : opposingPlayer,
						null);

					root.addChildNode(makeNode(root,
						deckCopy,
						finalBoard,
						(TerrainTile) finalBoard.at(tilePlacement.getPosition()),
						phasingPlayer,
						opposingPlayer));
				}

			}
			Player phasingPlayer = new Player(root.getOpposingPlayer());
			Player opposingPlayer = new Player(root.getPhasingPlayer());

			Utils.calcScore(tempBoard,
				tilePlacement,
				phasingPlayer.getColour() == Meeple.RED ? phasingPlayer : opposingPlayer,
				phasingPlayer.getColour() == Meeple.BLUE ? phasingPlayer : opposingPlayer,
				null);

			root.addChildNode(makeNode(root,
				deckCopy,
				tempBoard,
				tilePlacement,
				phasingPlayer,
				opposingPlayer));

		});

	}

	private Node expandOne(Node node) {

		LinkedList<TerrainTile> deckCopy = Utils.copyDeck(node.getRemainingDeck());

		if (deckCopy.size() == 0) { return null; }
		TerrainTile tile = new TerrainTile(deckCopy.poll());

		List<TerrainTile> possibleTilePlacements = Utils.CalculateTilePlacements(
			(GameBoard) node.getGameBoard(),
			tile);

		while (deckCopy.size() > 0 && possibleTilePlacements.size() == 0) {
			tile = new TerrainTile(deckCopy.poll());
		}

		if (possibleTilePlacements.size() == 0) { return null; }
		List<Node> options = new ArrayList<>();

		// Collections.shuffle(possibleTilePlacements);
		TerrainTile reference = possibleTilePlacements
			.get(new Random().nextInt(possibleTilePlacements.size()));

		GameBoard tempBoard = new GameBoard(node.getGameBoard());
		tempBoard.placeTile(reference.getPosition(),
			reference);

		// No reason to place meeple if no meeples
		// are available
		if (node.getOpposingPlayer().getRemainingMeeples() > 0) {

			for (Direction dir : Utils.calcPlaceableMeeples(tempBoard,
				reference)) {

				// this is the stuff that will be
				// stored in a child node.
				Player phasingPlayer = new Player(node.getOpposingPlayer());
				Player opposingPlayer = new Player(node.getPhasingPlayer());
				GameBoard finalBoard = new GameBoard(tempBoard);

				// changing the tempTile with the
				// same tile but with meeple
				finalBoard.placeMeeple(node.getOpposingPlayer().getColour(),
					dir,
					reference.getPosition());

				try {
					phasingPlayer.popMeeple();
				} catch (StackPushPullException e) {
					e.printStackTrace();
				}
				// TODO CALC SCORE
				Utils.calcScore(finalBoard,
					(TerrainTile) finalBoard.at(reference.getPosition()),
					phasingPlayer.getColour() == Meeple.RED ? phasingPlayer : opposingPlayer,
					phasingPlayer.getColour() == Meeple.BLUE ? phasingPlayer : opposingPlayer,
					null);

				Node child = makeNode(node,
					deckCopy,
					finalBoard,
					(TerrainTile) finalBoard.at(reference.getPosition()),
					phasingPlayer,
					opposingPlayer);
				options.add(child);

			}

		}
		Player phasingPlayer = new Player(node.getOpposingPlayer());
		Player opposingPlayer = new Player(node.getPhasingPlayer());

		Utils.calcScore(tempBoard,
			reference,
			phasingPlayer.getColour() == Meeple.RED ? phasingPlayer : opposingPlayer,
			phasingPlayer.getColour() == Meeple.BLUE ? phasingPlayer : opposingPlayer,
			null);

		Node child = makeNode(node,
			deckCopy,
			tempBoard,
			reference,
			phasingPlayer,
			opposingPlayer);

		options.add(child);

		Node nodeToAdd = options.get(new Random().nextInt(options.size()));
		node.addChildNode(nodeToAdd);

		return nodeToAdd;
	}

	private Node makeNode(Node parent, LinkedList<TerrainTile> remainingDeck, GameBoard board,
		TerrainTile tile, Player phasingPlayer, Player opposingPlayer) {
		GameState newState = new GameState(board, remainingDeck, phasingPlayer, opposingPlayer, parent.getState().getTurn()+1);

		Node newNode = new Node(newState);
		newNode.setAction(tile);
		newNode.setParent(parent);
		return newNode;
	}

	private Double rollout(GameBoard gb, LinkedList<TerrainTile> remainingDeck, Player phasingPlayer,
		Player opposingPlayer, int rolloutTurn) {

		int currentTurn = rolloutTurn;
		final int M = 10;
		List<PlayedTile> playedTiles = new ArrayList<>();

		while (remainingDeck.size() > 0) {
			onePly(gb,
				remainingDeck,
				phasingPlayer,
				opposingPlayer,
				playedTiles, ++currentTurn, M);
			Player temp = phasingPlayer;
			phasingPlayer = opposingPlayer;
			opposingPlayer = temp;
		}

		Utils.calcFinalScore(gb,
			phasingPlayer.getColour() == Meeple.RED ? phasingPlayer : opposingPlayer,
			phasingPlayer.getColour() == Meeple.BLUE ? phasingPlayer : opposingPlayer,
			null);

		
		double score = MCTSUtils.rewardPolicyWin(phasingPlayer,
			opposingPlayer,
			mainPlayerColour);
		
		
	
		for (PlayedTile pt : playedTiles) {
			historyMap.incrementScoreForAffected(pt, score);
		}
		

		return MCTSUtils.doScore(rewardPolicy, phasingPlayer, opposingPlayer, mainPlayerColour);

	}

	private void onePly(GameBoard gb, LinkedList<TerrainTile> remainingDeck, Player phasingPlayer,
		Player opposingPlayer, List<PlayedTile> playedTiles, int turn, int M) {
		RandomAgent ra = new RandomAgent();

		TerrainTile tileFromDeck = null;
		boolean done = false;
		while (!done) {
			tileFromDeck = remainingDeck.poll();
			List<TerrainTile> actions = Utils.CalculateTilePlacements(gb,
				tileFromDeck);
			if (!actions.isEmpty()) { done = true; }
		}

		Action action = ra.getAction(new GameBoard(gb),
			new TerrainTile(tileFromDeck),
			null,
			phasingPlayer,
			null);

		gb.placeTile(action.getTile().getPosition(),
			action.getTile());

		if (action.getMeepleDirection() != null) {
			action.getTile()
				.placeMeeple(phasingPlayer.getColour(),
					action.getMeepleDirection());
			try {
				phasingPlayer.popMeeple();
			} catch (StackPushPullException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		if(playedTiles.size() < M)
			playedTiles.add(historyMap.addIncrementAndReturn(new PlayedTile(action.getTile(), phasingPlayer, turn)));

		Utils.calcScore(gb,
			action.getTile(),
			phasingPlayer.getColour() == Meeple.RED ? phasingPlayer : opposingPlayer,
			phasingPlayer.getColour() == Meeple.BLUE ? phasingPlayer : opposingPlayer,
			null);

	}



	private void backPropagate(Node node, Double result) {
		if (node.getParent() == null) {
			updateStats(node,
				result);
			return;
		}
		updateStats(node,
			result);
		backPropagate(node.getParent(),
			result);
	}

	private void updateStats(Node node, Double result) {
		node.addVisitCount();
		node.addTotalScore(result);
	}

	private Node selectPromisingNode(Node root) {

		Node node = root;
		while (node.getChildNodes().size() != 0) {

			node = UCTProgressiveHistory.findBestNodeWithProgressiveHistory(node,
				UCTconstant, historyMap, CUTOFF, a);
		}

		if (node.getVisitCount() != 0 && node.getParent() != root) {
			Node parent = node.getParent();

			if (UCTProgressiveHistory.phValue(parent,
				UCTconstant, historyMap, CUTOFF, a) > UCTProgressiveHistory.phValue(node,
					UCTconstant, historyMap, CUTOFF, a)) {
				node = parent;
			}
		}

		return node;
	}

}
