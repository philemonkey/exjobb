package se.miun.pebe1700.dt133g.utils;

public class StackPushPullException extends Exception {

	  public StackPushPullException(String message){
	     super(message);
	  }

	}