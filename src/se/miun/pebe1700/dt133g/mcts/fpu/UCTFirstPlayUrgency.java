package se.miun.pebe1700.dt133g.mcts.fpu;

import java.util.Collections;
import java.util.Comparator;

import se.miun.pebe1700.dt133g.carcassonne.enums.Meeple;
import se.miun.pebe1700.dt133g.mcts.Node;

public class UCTFirstPlayUrgency {

	final static double FPU_CONSTANT = 0.15d;
	
	public static double fpuValue(Node n, double constant, Meeple mainPlayerColour) {
		
		int nodeVisit = n.getVisitCount();
		int totalVisit = n.getParent().getVisitCount();
		Double totalScore = n.getTotalScore();
		
		int toRoot = 0;
		
		Node tempNode = n;
		while(!tempNode.isRootNode()) {
			toRoot++;
			tempNode = tempNode.getParent();
		}
		
		//System.out.println(toRoot);
		
		if (nodeVisit == 0) {
			if(toRoot >= 3) {
				return FPU_CONSTANT;
			}
			else return Integer.MAX_VALUE;
		}
	

			
//		System.out.println(((double) totalScore / (double) nodeVisit)
//				+ constant * Math.sqrt((double) Math.log(totalVisit) / (double) nodeVisit));
		return ((double) totalScore / (double) nodeVisit)
				+ constant * Math.sqrt((double) Math.log(totalVisit) / (double) nodeVisit);
	}

	public static Node findBestNodeWithUCT(Node node, double constant, Meeple mainPlayerColour) {
		return Collections.max(node.getChildNodes(),
			Comparator.comparing(c -> fpuValue(c,
				constant, mainPlayerColour)));
	}
}