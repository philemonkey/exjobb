package se.miun.pebe1700.dt133g.mcts.pb_w_simcutoff;

import java.util.Collections;
import java.util.Comparator;

import se.miun.pebe1700.dt133g.carcassonne.Player;
import se.miun.pebe1700.dt133g.carcassonne.enums.Meeple;
import se.miun.pebe1700.dt133g.mcts.Node;

public class UCTProgressiveBias {

	public static double pbValue(Node n, double constant, Meeple mainPlayerColour) {

		int nodeVisit = n.getVisitCount();
		int totalVisit = n.getParent().getVisitCount();
		Double totalScore = n.getTotalScore();

		if (nodeVisit == 0) { return Integer.MAX_VALUE; }
		double utility = 0;
		
			
		utility = Eval.getEval(n.getGameBoard(),
			n.getPhasingPlayer(),
			n.getOpposingPlayer(),
			mainPlayerColour, n.getRemainingDeck().size());

//		if(utility == 0) {
//			System.out.println(utility);
//			double what = ((double) totalScore / (double) nodeVisit)
//				+ (constant * Math.sqrt((double) Math.log(totalVisit) / (double) nodeVisit)) + utility/1;
//			System.out.println("no util: " + what);
//		}
//		else {
//			System.out.println(utility);
//			double what = ((double) totalScore / (double) nodeVisit)
//				+ (constant * Math.sqrt((double) Math.log(totalVisit) / (double) nodeVisit)) + utility/1;
//			System.out.println("UTIL: " + what);
//		}

		// TODO don't have loss count.
		return ((double) totalScore / (double) nodeVisit)
			+ (constant * Math.sqrt((double) Math.log(totalVisit) / (double) nodeVisit))
			+ utility / (n.getLosses() +6);
	}

	public static Node findBestNodeWithProgressiveBias(Node node, double constant,
		Meeple mainPlayerColour) {
		return Collections.max(node.getChildNodes(),
			Comparator.comparing(c -> pbValue(c,
				constant,
				mainPlayerColour)));
	}

}