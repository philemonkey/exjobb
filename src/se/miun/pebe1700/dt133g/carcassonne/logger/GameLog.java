package se.miun.pebe1700.dt133g.carcassonne.logger;

import java.awt.Color;

import se.miun.pebe1700.dt133g.carcassonne.Player;
import se.miun.pebe1700.dt133g.carcassonne.Structure;
import se.miun.pebe1700.dt133g.carcassonne.TerrainTile;
import se.miun.pebe1700.dt133g.carcassonne.enums.Feature;
import se.miun.pebe1700.dt133g.carcassonne.enums.Meeple;
import se.miun.pebe1700.dt133g.carcassonne.gui.StatusPanel;

public class GameLog implements GameListener {

	String prefix, delimiter, suffix;

	public GameLog(String prefix, String delimiter, String suffix) {
		this.prefix = prefix;
		this.delimiter = delimiter;
		this.suffix = suffix;
	}

	public GameLog() {
		this(">", "#", "");
	}

	@Override
	public void tilePlaced(Player player, TerrainTile tile) {
		StatusPanel.showNewMessage(
				prefix + player.getColour() + " placed a tile at row: "
						+ (tile.getPosition().y() + 1) + " col: "
						+ (tile.getPosition().x() + 1) + suffix,
				player.getColour() == Meeple.RED ? Color.RED : Color.BLUE);

	}

	@Override
	public void meeplePlaced(Player player, TerrainTile tile, Feature feature) {
		StatusPanel.showNewMessage(
				prefix + player.getColour() + " placed a Meeple at row: "
						+ (tile.getPosition().y() + 1) + " col: "
						+ (tile.getPosition().x() + 1) + " on " + feature
						+ suffix,
				player.getColour() == Meeple.RED ? Color.RED : Color.BLUE);

	}

	@Override
	public void structureDone(String mess, Structure structure) {
		StatusPanel.showNewMessage(prefix + mess + structure.getFeature()
				+ " of size " + structure.getSize() + suffix, Color.YELLOW);

	}

	@Override
	public void meepleScore(Player player, Structure structure) {
		// TODO Auto-generated method stub

	}

	@Override
	public void playerStatus(Player player) {
		StatusPanel.showNewMessage(
				prefix + player.getColour() + " SCORE: " + player.getScore()
						+ ", MEEPLES: " + player.getRemainingMeeples(),
				Color.WHITE);

	}

	@Override
	public void finalScore(Player player1, Player player2) {
		StatusPanel.showNewMessage(prefix + player1.getColour()
				+ " FINAL SCORE: " + player1.getScore() + "\n"
				+ player2.getColour() + " FINAL SCORE: " + player2.getScore(),
				Color.WHITE);

	}
	
	@Override
	public void generalMessage(String mess) {
		StatusPanel.showNewMessage(prefix + mess,
				Color.WHITE);

	}
	
	

}