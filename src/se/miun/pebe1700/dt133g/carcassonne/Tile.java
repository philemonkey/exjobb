package se.miun.pebe1700.dt133g.carcassonne;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import se.miun.pebe1700.dt133g.carcassonne.enums.Direction;
import se.miun.pebe1700.dt133g.utils.Point;

public abstract class Tile {


	protected Point position;
	protected Map<Direction, Tile> neighbours;
	
	protected Tile() {
		neighbours = new HashMap<>();
	}
	
	
	
	public void setPosition(Point p) {
		this.position = p;
	}
	
	public Point getPosition() {
		return this.position;
	}
	
	public void incrementPosition() {
		this.position.increment(1);
	}
	
	public void decrementPosition() {
		this.position.decrement(1);
	}
	
	public void setNeighbour(Direction direction, Tile tile) {
		if(neighbours.containsKey(direction)) {
			neighbours.replace(direction, tile);
		}
		else {
			neighbours.put(direction, tile);
		}
			
		
		
	}
	
	public Map<Direction, Tile> getNeighbours() {
		return Collections.unmodifiableMap(neighbours);
	}
	
	public Tile getNeighbourInDirecton(Direction dir) {
		return Collections.unmodifiableMap(neighbours).get(dir);
	}
}
