package se.miun.pebe1700.dt133g.carcassonne.gui;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.swing.JPanel;

import se.miun.pebe1700.dt133g.carcassonne.GameBoard;
import se.miun.pebe1700.dt133g.carcassonne.JustToCheckBoard;
import se.miun.pebe1700.dt133g.carcassonne.TerrainTile;

@SuppressWarnings("serial")
public class PlayingField extends JPanel {


	private Cell[][] cells;
	private int size;


	public PlayingField() {
		setBackground(Color.decode(GameColors.CELL_BACKGROUND));

	}

	public void makeBoard(GameBoard board) {

		this.removeAll();
		
		this.size = board.getSize();
		cells = new Cell[size][size];
		
		setLayout(new GridBagLayout());
		GridBagConstraints gc = new GridBagConstraints();

		gc.weighty = 1;
		gc.weightx = 1;
		gc.fill = GridBagConstraints.BOTH;
		for (int row = 0; row < size; row++) {
			for (int col = 0; col < size; col++) {
				gc.gridy = row;
				gc.gridx = col;
				Cell cell = new Cell(row, col);
				if(board.getOccupied(row, col)) {
					cell.placeTile(board.getTerrainTile(row, col));
				}
				add(cell, gc);
				cells[row][col] = cell;
				
			}
		}
		revalidate();
		repaint();
	}
	
	public void placeTile(int row, int col, TerrainTile tile) {
		cells[row][col].placeTile(tile);
	}


}
