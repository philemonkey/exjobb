package se.miun.pebe1700.dt133g.mcts.ph;

import java.util.Collections;
import java.util.Comparator;

import se.miun.pebe1700.dt133g.mcts.Node;

public class UCTProgressiveHistory {

	public static double phValue(Node n, double constant, HistoryMap historyMap, int CUTOFF, int iterCount) {

		int nodeVisit = n.getVisitCount();
		int totalVisit = n.getParent().getVisitCount();
		Double totalScore = n.getTotalScore();

		ScoreVisits scoreVisits = historyMap
			.getScoreVisitFromTile(new PlayedTile(n.getAction(), n.getPhasingPlayer(), n.getState().getBoard().getSize()));

		if (nodeVisit == 0) { return Integer.MAX_VALUE; }
		double tileFactor = 0;
		if(iterCount < CUTOFF) {
			tileFactor = scoreVisits == null ? 0d
					: scoreVisits.getScore() / (double) scoreVisits.getVisits() * 1
						/ ((double) (nodeVisit - totalScore + 1d));
		}
	

//		if(scoreVisits != null) {
//			System.out.println("HAS HISTORY, tileFactor: " + tileFactor);
//			System.out.println("totalScore: " + totalScore + " nodeVisit:" + nodeVisit);
//			System.out.println((((double) totalScore / (double) nodeVisit)
//				+ constant * Math.sqrt((double) Math.log(totalVisit) / (double) nodeVisit) + tileFactor));
//			System.out.println(((double) totalScore / (double) nodeVisit)
//				+ constant * Math.sqrt((double) Math.log(totalVisit) / (double) nodeVisit));
//		}
//		else {
//			System.out.println("no history");
//			System.out.println(((double) totalScore / (double) nodeVisit)
//				+ constant * Math.sqrt((double) Math.log(totalVisit) / (double) nodeVisit) + tileFactor);
//		}

//		if(tileFactor >= 0.8d) {
//			System.out.println("tileFactor used");
//		}
		if(iterCount < CUTOFF) {
			return ((double) totalScore / (double) nodeVisit)
					+ constant * Math.sqrt((double) Math.log(totalVisit) / (double) nodeVisit) + tileFactor;
		}
		else {
			return ((double) totalScore / (double) nodeVisit)
					+ constant * Math.sqrt((double) Math.log(totalVisit) / (double) nodeVisit);
		}

	}

	public static Node findBestNodeWithProgressiveHistory(Node node, double constant,
		HistoryMap historyMap, int CUTOFF, int iterCount) {
		return Collections.max(node.getChildNodes(),
			Comparator.comparing(c -> phValue(c,
				constant,
				historyMap, CUTOFF, iterCount)));
	}
}