package se.miun.pebe1700.dt133g.carcassonne.gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JPanel;

import se.miun.pebe1700.dt133g.carcassonne.PartialStructure;
import se.miun.pebe1700.dt133g.carcassonne.TerrainTile;
import se.miun.pebe1700.dt133g.carcassonne.enums.Direction;
import se.miun.pebe1700.dt133g.carcassonne.enums.Meeple;
import se.miun.pebe1700.dt133g.utils.Point;

@SuppressWarnings("serial")
public class Cell extends JPanel {

	private int row, col;
	private boolean playable;
	private boolean occupied;
	private TerrainTile tile;

	public Cell(int row, int col) {
		this.row = row;
		this.col = col;

		// setPreferredSize(new Dimension(50, 50));
		setBorder(BorderFactory
				.createLineBorder(Color.decode(GameColors.CELL_OUTER_STROKE)));

	}

	public Point getPosition() {
		return new Point(col, row);
	}

	public boolean placeTile(TerrainTile tile) {
		if (!occupied) {
			this.occupied = true;
			this.tile = tile;

			repaint();
			return true;
		}
		return false;

	}

	@Override
	protected void paintComponent(Graphics g) {

		super.paintComponent(g);
		if (occupied) { doDrawing(g); }

	}

	private void doDrawing(Graphics g) {

		var g2d = (Graphics2D) g;
		var size = getSize();

		g2d.setColor(GameColors.FIELDS);

		g2d.fillRect(0, 0, size.width, size.height);

		for (PartialStructure part : tile.getPartialStructures()) {
			switch (part.getFeature()) {
			case CITY:
				paintCity(g2d, part.getDirections(), size);
				break;

			case ROAD:
				paintRoad(g2d, part.getDirections(), size);
				break;
			default:
				break;

			}
			if (part.getMeeple() != null) {
				paintMeeple(g2d, part.getMeeple(), part.getMeepleDirection(), size);
			}
		
		}

		switch (tile.getCenterFeature()) {
		case CLOISTER:
			g2d.setColor(GameColors.CLOISTER);
			g2d.fillRect(size.width / 2 - size.width / 12,
					size.height / 2 - size.height / 12, size.width / 6,
					size.height / 6);
			break;

		case CROSSING:
			g2d.setColor(Color.DARK_GRAY);
			g2d.fillRect(size.width / 2 - size.width / 12,
					size.height / 2 - size.height / 12, size.width / 6,
					size.height / 6);

			break;
		case CITY:
			g2d.setColor(GameColors.CITY);
			g2d.fillRect(size.width / 4, size.height / 4, size.width / 2,
					size.height / 2);

			break;
		default:
			break;

		}
	}

	private void paintMeeple(Graphics2D g2d, Meeple meeple, Direction dir,
			Dimension size) {

		final int MEEP_SIZE = size.width / 5;
		final int MEEP_HALFSIZE = MEEP_SIZE / 2;

		g2d.setColor(meeple == Meeple.RED ? Color.RED : Color.BLUE);

		switch (dir) {
		case CENTER:
			g2d.fillOval(size.width / 2 - MEEP_HALFSIZE,
					size.height / 2 - MEEP_HALFSIZE, MEEP_SIZE, MEEP_SIZE);
			break;
		case NORTH:
			g2d.fillOval(size.width / 2 - MEEP_HALFSIZE,
					MEEP_SIZE - MEEP_HALFSIZE, MEEP_SIZE, MEEP_SIZE);
			break;
		case EAST:
			g2d.fillOval(size.width - MEEP_SIZE - MEEP_HALFSIZE,
					size.height / 2 - MEEP_HALFSIZE, MEEP_SIZE, MEEP_SIZE);
			break;
		case SOUTH:
			g2d.fillOval(size.width / 2 - MEEP_HALFSIZE,
					size.height - MEEP_SIZE - MEEP_HALFSIZE, MEEP_SIZE,
					MEEP_SIZE);
			break;
		case WEST:
			g2d.fillOval(0 + MEEP_HALFSIZE, size.height / 2 - MEEP_HALFSIZE,
					MEEP_SIZE, MEEP_SIZE);
			break;
			/* ------------------------------------ */
			// TO BE IMPLEMENTED FOR FIELDS
			/* ------------------------------------*/
//		case NORTH_WEST:
//			g2d.fillOval(0 + MEEP_HALFSIZE, 0 + MEEP_HALFSIZE, MEEP_SIZE,
//					MEEP_SIZE);
//			break;
//		case NORTH_EAST:
//			g2d.fillOval(size.width - MEEP_SIZE - MEEP_HALFSIZE,
//					0 + MEEP_HALFSIZE, MEEP_SIZE, MEEP_SIZE);
//			break;
//		case SOUTH_EAST:
//			g2d.fillOval(size.width - MEEP_SIZE - MEEP_HALFSIZE,
//					size.height - MEEP_SIZE - MEEP_HALFSIZE, MEEP_SIZE,
//					MEEP_SIZE);
//			break;
//		case SOUTH_WEST:
//			g2d.fillOval(0 + MEEP_HALFSIZE,
//					size.height - MEEP_SIZE - MEEP_HALFSIZE, MEEP_SIZE,
//					MEEP_SIZE);
//			break;
		default:
			break;

		}

	}

	private void paintCity(Graphics2D g2d, List<Direction> directions,
			Dimension size) {
		g2d.setColor(GameColors.CITY);
		int xpoints[] = null;
		int ypoints[] = null;
		int margin = directions.size() == 1 ? size.width / 3 : size.width / 4;

		for (Direction dir : directions) {
			switch (dir) {
			case NORTH:
				xpoints = new int[] { 0, margin, size.width - margin,
						size.width };
				ypoints = new int[] { 0, size.height / 4, size.height / 4, 0 };
				break;
			case EAST:
				xpoints = new int[] { size.width, size.width - size.width / 4,
						size.width - size.width / 4, size.width };
				ypoints = new int[] { 0, margin, size.height - margin,
						size.height };
				break;
			case SOUTH:

				xpoints = new int[] { 0, margin, size.width - margin,
						size.width };
				ypoints = new int[] { size.height,
						size.height - size.height / 4,
						size.height - size.height / 4, size.height };
				break;
			case WEST:
				xpoints = new int[] { 0, size.width / 4, size.width / 4, 0 };
				ypoints = new int[] { 0, margin, size.height - margin,
						size.height };
				break;
			default:
				break;
			}

			g2d.fillPolygon(xpoints, ypoints, 4);
		}

	}

	private void paintRoad(Graphics2D g2d, List<Direction> directions,
			Dimension size) {
		g2d.setColor(GameColors.ROAD);
		final int ROAD_WIDTH = size.width / 10;
		for (Direction dir : directions) {

			switch (dir) {
			case NORTH:
				g2d.fillRect(size.width / 2 - ROAD_WIDTH / 2, 0, ROAD_WIDTH,
						size.height / 2);
				break;
			case EAST:
				g2d.fillRect(size.width - size.width / 2,
						size.height / 2 - ROAD_WIDTH / 2, size.width / 2,
						ROAD_WIDTH);
				break;
			case SOUTH:
				g2d.fillRect(size.width / 2 - ROAD_WIDTH / 2,
						size.height - size.height / 2, ROAD_WIDTH,
						size.height / 2);
				break;
			case WEST:
				g2d.fillRect(0, size.height / 2 - ROAD_WIDTH / 2,
						size.width / 2, ROAD_WIDTH);
				break;
			default:
				break;
			}
		}
	}

	public boolean getOccupied() {
		return this.occupied;
	}

	public boolean getPlayable() {
		return this.playable;
	}

}
