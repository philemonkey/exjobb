package se.miun.pebe1700.dt133g.carcassonne.agents;


import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.CompletionService;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import se.miun.pebe1700.dt133g.carcassonne.Action;
import se.miun.pebe1700.dt133g.carcassonne.GameBoard;
import se.miun.pebe1700.dt133g.carcassonne.Player;
import se.miun.pebe1700.dt133g.carcassonne.TerrainTile;
import se.miun.pebe1700.dt133g.carcassonne.enums.Direction;
import se.miun.pebe1700.dt133g.mcts.MCTSChunk;
import se.miun.pebe1700.dt133g.mcts.MonteCarloTreeSearch;

public class MCTSAgent extends Agent {

	private final int N_DETER;
	private final int POOL_SIZE;
	private ExecutorService executor;
	private CompletionService<TerrainTile> completionService;
	private List<TerrainTile> tiles;
	private List<MonteCarloTreeSearch> nAgents;

	public MCTSAgent(List<MonteCarloTreeSearch> nAgents, int poolSize) {
		this.POOL_SIZE = poolSize;
		tiles = new ArrayList<>();
		this.nAgents = nAgents;
		this.N_DETER = this.nAgents.size();

	}
	

	@Override
	public Action getAction(GameBoard board, TerrainTile tileFromDeck, LinkedList<TerrainTile> deck,
		Player phasingPlayer, Player opposingPlayer) {
		return null;
	}

	@Override
	public Action getAction() {

		executor = Executors.newFixedThreadPool(POOL_SIZE);
		completionService = new ExecutorCompletionService<TerrainTile>(executor);
//		System.out.print("ThreadPoolStrategy using " + POOL_SIZE + " threads ");
//		if (POOL_SIZE == Runtime.getRuntime().availableProcessors())
//			System.out.print("== #available processor cores");
//		System.out.println("\n" + N_DETER + " Determinizations");


		// TODO now the same MCTS-instance goes through all iterations
		for (int i = 0; i < N_DETER; ++i) {

			Callable<TerrainTile> worker = new MCTSChunk(nAgents.get(i));
			completionService.submit(worker);
		}

	
		for (int i = 0; i < N_DETER; ++i) {
			try {
				tiles.add(completionService.take().get());
			} catch (InterruptedException | ExecutionException e) {
				e.printStackTrace();
			}
		}

		executor.shutdown();
		
		int[] freq = new int[N_DETER];
		for(int i = 0; i < N_DETER; i++) {
			for(TerrainTile t: tiles) {
				if(tiles.get(i).sameTile(t)) {
					freq[i]++;
				}
			}
		}
	
		TerrainTile tile = tiles.get(findLargest(freq));
		Direction dir = tile.getMeepleDirection();
		tile.removeMeeple();
		return new Action(tile, dir);
	}
	
	private int findLargest(int[] a) {
		int max = a[0];
		int index = 0;

		for (int i = 0; i < a.length; i++) 
		{
			if (max < a[i]) 
			{
				max = a[i];
				index = i;
			}
		}
		return index;
	}

}
