package se.miun.pebe1700.dt133g.mcts;

import se.miun.pebe1700.dt133g.carcassonne.Player;
import se.miun.pebe1700.dt133g.carcassonne.enums.Meeple;

public class MCTSUtils {
	
	
	public static Double doScore(RewardPolicy rew, Player phasingPlayer, Player opposingPlayer, Meeple mainPlayerColour) {
		switch (rew) {
		case WIN:
			return MCTSUtils.rewardPolicyWin(phasingPlayer,
				opposingPlayer,
				mainPlayerColour);
		case SCORE:
			return MCTSUtils.rewardPolicyScore(phasingPlayer,
				opposingPlayer,
				mainPlayerColour);
		case SCORE_AND_WIN:
			return MCTSUtils.rewardPolicyScoreAndWin(phasingPlayer,
				opposingPlayer,
				mainPlayerColour);
		case SCORE_BOTH:
			return MCTSUtils.rewardPolicyScoreBoth(phasingPlayer,
				opposingPlayer,
				mainPlayerColour);
		}
		return null;
	}

	public static double rewardPolicyWin(Player phasingPlayer, Player opposingPlayer, Meeple mainPlayerColour) {
		if (phasingPlayer.getColour() == mainPlayerColour) {
			if (phasingPlayer.getScore() > opposingPlayer.getScore())
				return 1d;
			else if (phasingPlayer.getScore() < opposingPlayer.getScore())
				return -1d;
			else
				return 0d;
		} else {
			if (phasingPlayer.getScore() < opposingPlayer.getScore())
				return 1d;
			else if (phasingPlayer.getScore() > opposingPlayer.getScore())
				return -1d;
			else
				return 0d;
		}
	}

	public static double rewardPolicyScoreAndWin(Player phasingPlayer, Player opposingPlayer, Meeple mainPlayerColour) {
		if (phasingPlayer.getColour() == mainPlayerColour) {
			if (phasingPlayer.getScore() > opposingPlayer.getScore()) {
				return (double) phasingPlayer.getScore() / 200;
			}
			return 0;

		} else {
			if (opposingPlayer.getScore() > phasingPlayer.getScore()) {
				return (double) opposingPlayer.getScore() / 200;
			}
			return 0;
		}
	}

	public static double rewardPolicyScore(Player phasingPlayer, Player opposingPlayer, Meeple mainPlayerColour) {
		if (phasingPlayer.getColour() == mainPlayerColour) {
			return (double) phasingPlayer.getScore() / 200;
		} else {
			return (double) opposingPlayer.getScore() / 200;
		}
	}
	
	public static double rewardPolicyScoreBoth(Player phasingPlayer, Player opposingPlayer, Meeple mainPlayerColour) {
		double mainPlayerScore;
		double oppPlayerScore;
		if (phasingPlayer.getColour() == mainPlayerColour) {
			mainPlayerScore = (double) phasingPlayer.getScore() / 200;
			oppPlayerScore = (double) opposingPlayer.getScore() / 200;
		} else {
			mainPlayerScore = (double) opposingPlayer.getScore() / 200;
			oppPlayerScore = (double) phasingPlayer.getScore() / 200;
		}
		return mainPlayerScore - (oppPlayerScore-mainPlayerScore);
	}

}
