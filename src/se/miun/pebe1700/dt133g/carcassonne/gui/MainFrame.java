package se.miun.pebe1700.dt133g.carcassonne.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.util.List;
import java.util.concurrent.ExecutionException;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingWorker;

import se.miun.pebe1700.dt133g.carcassonne.GameBoard;
import se.miun.pebe1700.dt133g.carcassonne.GameTester;

@SuppressWarnings("serial")
public class MainFrame extends JFrame {

	private PlayingField playingField;
	private Container container;
	private GameTester game;
	private StatusPanel statusPanel;
	private JPanel agentTypes;


	// TODO Move shit to dedicated method.
	public MainFrame() {


		setSize(1500,
			1000);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		// setResizable(false);
		setTitle("Carcassonne!");
		setBackground(Color.RED);
		setLayout(new BorderLayout());

		container = getContentPane();

		JPanel leftSide = new JPanel();
		leftSide.setPreferredSize(new Dimension(1000, getHeight()));

		JPanel rightSide = new JPanel();
		rightSide.setPreferredSize(new Dimension(500, getHeight()));

		container.add(leftSide,
			BorderLayout.WEST);

		container.add(rightSide,
			BorderLayout.EAST);

		leftSide.setLayout(new BorderLayout());

		agentTypes = new JPanel();

		agentTypes.setBackground(Color.CYAN);
		agentTypes.setPreferredSize(new Dimension(leftSide.getWidth(), 50));
		leftSide.add(agentTypes,
			BorderLayout.NORTH);

		playingField = new PlayingField();
		playingField.setBackground(Color.DARK_GRAY);
		playingField.setPreferredSize(new Dimension(leftSide.getWidth(), leftSide.getHeight()));
		leftSide.add(playingField,
			BorderLayout.CENTER);

		rightSide.setLayout(new BorderLayout());

		statusPanel = new StatusPanel();
		statusPanel.setPreferredSize(new Dimension(rightSide.getWidth(), rightSide.getHeight()));
		rightSide.add(statusPanel,
			BorderLayout.CENTER);

		setJMenuBar(Menu.createMenuBar(this));
	}

	public void initGame(List<AgentSetup> setup) {

		game = new GameTester(setup);
		game.startGame();

		JLabel redLabel = new JLabel();
		JLabel blueLabel = new JLabel();

		redLabel.setText(makeLabel("RED: ", setup.get(0)));
		blueLabel.setText(makeLabel("BLUE: ", setup.get(1)));
		
		redLabel.setForeground(Color.RED);
		blueLabel.setForeground(Color.BLUE);
		
		agentTypes.add(redLabel, BorderLayout.LINE_START);
		agentTypes.add(blueLabel, BorderLayout.LINE_END);
		

		GetMove getMove = new GetMove();
		getMove.execute();

	}

	private String makeLabel(String color, AgentSetup as) {
		return color + as.AGENT_TYPE + ", " +  (as.ITERATIONS == 0 ? "Time: " + as.TIME_LIMIT + "ms" : "Iter: " + as.ITERATIONS)  + ", random deck: " + as.RANDOM_DECK
			+ ", reward policy: " + as.REWARD_POLICY;

	}

	public class GetMove extends SwingWorker<Boolean, String> {
		@Override
		protected Boolean doInBackground() {
			return game.makeMoveTest();
		}

		@Override
		protected void done() {
			try {
				Boolean res = get();

				if(res) {
					updateGUI(game.getBoard());
					GetMove getMove = new GetMove();
					getMove.execute();
				} else {
					game.startGame();
					GetMove getMove = new GetMove();
					getMove.execute();
				}
			} catch (InterruptedException | ExecutionException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
	};

	public void showMessage(String s) {
		JOptionPane.showMessageDialog(this,
			s);
	}

	public void updateGUI(GameBoard board) {
		playingField.makeBoard(board);
	}

}
