package se.miun.pebe1700.dt133g.carcassonne.agents;

import java.util.LinkedList;

import se.miun.pebe1700.dt133g.carcassonne.Action;
import se.miun.pebe1700.dt133g.carcassonne.GameBoard;
import se.miun.pebe1700.dt133g.carcassonne.Player;
import se.miun.pebe1700.dt133g.carcassonne.TerrainTile;

public abstract class Agent {

	public abstract Action getAction(GameBoard board, TerrainTile tileFromDeck,
			LinkedList<TerrainTile> deck, Player phasingPlayer,
			Player opposingPlayer);
	
	public abstract Action getAction();
}
