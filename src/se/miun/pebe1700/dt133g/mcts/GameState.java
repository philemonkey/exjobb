package se.miun.pebe1700.dt133g.mcts;

import java.util.LinkedList;

import se.miun.pebe1700.dt133g.carcassonne.GameBoard;
import se.miun.pebe1700.dt133g.carcassonne.Player;
import se.miun.pebe1700.dt133g.carcassonne.TerrainTile;


public class GameState {

	private GameBoard board;
	private LinkedList<TerrainTile> deck;
	private Player phasingPlayer;
	private Player opposingPlayer;
	private int turn;
	
	public GameState(GameBoard board, LinkedList<TerrainTile> deck, Player phasingPlayer, Player opposingPlayer, int turn) {
		this.board = board;
		this.deck = deck;
		this.phasingPlayer = phasingPlayer;
		this.opposingPlayer = opposingPlayer;
		this.turn = turn;
	}
	
	public GameBoard getBoard() {
		// TODO this is getting away
		return board;
	}
	
	public int getTurn() {
		return this.turn;
	}
	
	public LinkedList<TerrainTile> getDeck() {
		// TODO is it ok to let it get away?
		return deck;
	}
	
	public Player getPhasingPlayer() {
		return phasingPlayer;
	}
	
	public Player getOpposingPlayer() {
		return opposingPlayer;
	}
	
}
