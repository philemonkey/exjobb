package se.miun.pebe1700.dt133g.mcts;

import java.util.Collections;
import java.util.Comparator;

public class UCT {

	public static double uctValue(Node n, double constant) {
		
		int nodeVisit = n.getVisitCount();
		int totalVisit = n.getParent().getVisitCount();
		Double totalScore = n.getTotalScore();
		
		
		if (nodeVisit == 0) { return Integer.MAX_VALUE; }
	
		return ((double) totalScore / (double) nodeVisit)
				+ constant * Math.sqrt((double) Math.log(totalVisit) / (double) nodeVisit);
	}

	public static Node findBestNodeWithUCT(Node node, double constant) {
		return Collections.max(node.getChildNodes(),
			Comparator.comparing(c -> uctValue(c,
				constant)));
	}
}