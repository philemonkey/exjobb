package se.miun.pebe1700.dt133g.carcassonne.enums;

public enum Feature {
	CITY, ROAD, FIELDS, CROSSING, CLOISTER, NONE
}
