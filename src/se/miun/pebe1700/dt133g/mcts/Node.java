package se.miun.pebe1700.dt133g.mcts;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import se.miun.pebe1700.dt133g.carcassonne.GameBoard;
import se.miun.pebe1700.dt133g.carcassonne.Player;
import se.miun.pebe1700.dt133g.carcassonne.TerrainTile;



/*
 * The Node class is used by search algorithms to store the search state. 
 * A node points to a particular state of the search space. This state 
 * can be any Object subclass instance (String, Integer, etc.), as defined 
 * in the particular problem to solve.
 */
public class Node {


	private GameState state;
	private Node parent;
	private Double totalScore;
	private int losses;
	private TerrainTile action;
	private int nVisits;
	private List<Node> childNodes;
	private int turn;

	public Node(GameState state) {
		this.state = state;
		this.totalScore = 0.0d;
		this.nVisits = 0;
		this.losses = 0;
		childNodes = new ArrayList<>();
	}
	
	public Node(GameBoard board, LinkedList<TerrainTile> deck, Player phasingPlayer, Player opposingPlayer, int turn) {
		this(new GameState(board, deck, phasingPlayer, opposingPlayer, turn));
	}


	
	public void setAction(TerrainTile action) {
		this.action = action;
	}

	public Player getPhasingPlayer() {
		return state.getPhasingPlayer();
	}
	
	public int getLosses() {
		return this.losses;
	}
	
	public void incrementLoss() {
		this.losses++;
	}
	
	public Player getOpposingPlayer() {
		return state.getOpposingPlayer();
	}
	
	public LinkedList<TerrainTile> getRemainingDeck() {
		return state.getDeck();
	}
	
	public GameBoard getGameBoard() {
		return state.getBoard();
	}
	

	public void setParent(Node parent) {
		this.parent = parent;
	}

	public GameState getState() {
		return state;
	}
	
	public TerrainTile getAction() {
		return this.action;
	}


	public Node getParent() {
		return parent;
	}


	public boolean isRootNode() {
		return parent == null;
	}
	
	public void addChildNode(Node n) {
		childNodes.add(n);
	}
	
	public int getVisitCount() {
		return this.nVisits;
	}
	
	public Double getTotalScore() {
		return this.totalScore;
	}
	
	public void addVisitCount() {
		nVisits++;
	}
	
	public void addTotalScore(Double result) {
		totalScore += result;
	}
	
	public List<Node> getChildNodes() {
		return Collections.unmodifiableList(childNodes);
	}
	

	// backtracking, i guess
	public List<Node> getPathFromRoot() {
		List<Node> path = new ArrayList<Node>();
		Node current = this; // this node should be included
		while (!current.isRootNode()) {
			path.add(0, current); // shifting all other nodes to the right.
			current = current.getParent();
		}
		// ensure the root node is added
		path.add(0, current);
		return path;
	}

	public String toString() {
		return "\naction=" + getAction() +  "\nstate=" + getState() + "]";
	}

	public String pathToString() {
		String s = "";
		List<Node> nodes = getPathFromRoot();
		for (int i = 0; i < nodes.size(); i++) {
			System.out.println("State  : " + nodes.get(i).getState());
		}
		return s;
	}
}
