package se.miun.pebe1700.dt133g.carcassonne;

import java.util.HashMap;
import java.util.Map;

import se.miun.pebe1700.dt133g.carcassonne.enums.Direction;
import se.miun.pebe1700.dt133g.carcassonne.enums.Feature;

public class EmptyTile extends Tile {

	private boolean placeable;
	public EmptyTile(boolean placeable) {
		this.placeable = placeable;
	}
	

	
	public void setPlaceable(boolean placeable) {
		this.placeable = placeable;
	}
	
	public boolean getPlaceable() {
		return this.placeable;
	}
	
	
	public String toString() {
		return placeable ? "P" : "-";
	}
	
	public Map<Direction, Feature> getOuterFeatureInEachDirection() {
		Map<Direction, Feature> map = new HashMap<>();
		
		for(int i = 0; i < Utils.BASE_DIRECTIONS.length ; i++) {
			if(getNeighbours().get(Utils.BASE_DIRECTIONS[i]) instanceof TerrainTile) {
				TerrainTile tile = (TerrainTile) getNeighbours().get(Utils.BASE_DIRECTIONS[i]);
				
				map.put(Utils.BASE_DIRECTIONS[i], tile.getFeatureFromDirection(Utils.BASE_DIRECTIONS[(i+2)%4]));
			}
		}
		return map;
	}
	
	
	
}
