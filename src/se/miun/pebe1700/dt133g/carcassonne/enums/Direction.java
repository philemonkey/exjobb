package se.miun.pebe1700.dt133g.carcassonne.enums;



public enum Direction {
	NORTH, EAST, SOUTH, WEST, NORTH_WEST, NORTH_EAST, EAST_NORTH, EAST_SOUTH, SOUTH_EAST, SOUTH_WEST, WEST_SOUTH, WEST_NORTH, CENTER
}

