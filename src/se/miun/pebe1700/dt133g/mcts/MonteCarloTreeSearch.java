package se.miun.pebe1700.dt133g.mcts;

import java.util.LinkedList;

import se.miun.pebe1700.dt133g.carcassonne.GameBoard;
import se.miun.pebe1700.dt133g.carcassonne.Player;
import se.miun.pebe1700.dt133g.carcassonne.TerrainTile;
import se.miun.pebe1700.dt133g.carcassonne.Utils;
import se.miun.pebe1700.dt133g.carcassonne.enums.Meeple;
import se.miun.pebe1700.dt133g.utils.Stopwatch;

public abstract class MonteCarloTreeSearch {

	protected Node root;
	protected Meeple mainPlayerColour;
	protected TerrainTile tileFromDeck;
	protected final int N_ITERATIONS;
	protected final int TIME_LIMIT;
	protected RewardPolicy rewardPolicy;
	protected double UCTconstant;
	protected GameBoard board;
	protected LinkedList<TerrainTile> deck;
	protected Stopwatch stopwatch;


	public MonteCarloTreeSearch(GameBoard board, TerrainTile tileFromDeck,
		LinkedList<TerrainTile> deck, Player mainPlayer, Player otherPlayer, int nIterations, int timeLimit,
		RewardPolicy rewardPolicy, double UCTconstant) {
		
		
		root = new Node(new GameBoard(board), Utils.copyDeck(deck), new Player(otherPlayer), new Player(mainPlayer), 0);
		mainPlayerColour = mainPlayer.getColour();
		N_ITERATIONS = nIterations;
		TIME_LIMIT = timeLimit;
		this.rewardPolicy = rewardPolicy;
		this.tileFromDeck = tileFromDeck;
		this.UCTconstant = UCTconstant;
	}
	
	
	

	public abstract TerrainTile getNextMove();

}
